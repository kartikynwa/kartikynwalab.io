# kartikynwa.gitlab.io

Built using [Zola](https://getzola.org).

## Creating and publishing the zola container

For some reason, the zola image from ghcr.io does not work out of the box in GitLab's CI. The workaround is to take the `zola` binary from the official container image and installing it to a Debian image. This can be automated as follows but there might be pitfalls I am not aware of:

```bash
# replace 0.17.0 with the desired zola version
# from https://github.com/getzola/zola/pkgs/container/zola
./publish_zola_container.sh 0.17.0
```

Then update `.gitlab-ci.yml` file to use the published container image.

Feel free to remove the container image locally that is created as a result.
