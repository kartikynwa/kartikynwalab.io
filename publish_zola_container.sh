#!/usr/bin/env bash

set -e

zola_version="$1"

if [ -z "$zola_version" ]; then
  >&2 echo "USAGE: ./zola_container.sh ZOLA_VERSION"
  exit 1
fi

image_name="registry.gitlab.com/kartikynwa/kartikynwa.gitlab.io:zola-v$zola_version"

cat <<EOF | sed "s/{ZOLA_VERSION}/$zola_version/" | podman build -t "$image_name" -f - .
FROM ghcr.io/getzola/zola:v{ZOLA_VERSION} AS zola

FROM docker.io/debian:stable-slim
COPY --from=zola /bin/zola /bin/zola
EOF

# podman push "$image_name"
