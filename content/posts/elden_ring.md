+++
title = "Elden Ring"
date = "2023-07-04"

[taxonomies]
Tags=["non-political"]
+++

# Elden: The Ring

## How It Began

Having bought into the hype surrounding *Tears of the Kingdom* and by the
lawlessness of its leak before the official release, I began to sink my
worthless hours into it.

The game features new mechanics that are cool and creative. These make for some
great, fun puzzles. But at its core, it is a sequel to *Breath of the Wild*.
The game is fundamentally childish in its art style and narration. It reminds
me of this quote:

> "I said round about 2011 that I thought that it had serious and worrying
> implications for the future if millions of adults were queueing up to see
> Batman movies," \[Alan Moore\] said. "Because that kind of
> infantilization—that urge towards simpler times, simpler realities—that can
> very often be a precursor to fascism."

Now, I don't know how Alan Moore understands fascism. Admittedly I don't
understand fascism too well either. But something about this statement of his
resonates with me (could be confirmation bias). This game and games like this
are lazy, undialectical and somewhat fascistic about how they depict conflicts
and the morality surrounding it. There are ontologically good and evil sides.
Evil side has hideous goblins that are ripe for righteous culling. They are
clearly evil because they look abhorrent and different from you, the good guy,
and people like you. That is your cue to wipe them out and loot the treasure
they are guarding. In interest of clarity, I am not suggesting that there is a
heretical reading of this game's setting where the villain and his army are
fighting a justified war but have been misconstrued because the story is being
told from a specific, biased perspective. In fact, I have no doubt that the
villain should be defeated. But it is this foundation of the premise that is
lazily crafted that I take issue with. I don't mind children's stories being
like this but this is a game which is primarily consumed by adults
uncritically. The target adults also live in imperial core countries with a
colonial past. As it happens, the wars that imperial countries wage on the
Global South are also sold to their citizens wrapped in a good-versus-evil
rhetoric. I am already nips-deep in fascism and I don't need any more of it.

There are examples of media that look like they are oriented towards children
at least in outward appearance. But there is some depth to them that is useful
and stimulating for adults. Studio Ghibli films for example. Nothing like that
in this game though. Just bash these goblins and monsters. The dialogue sounds
straight out of fairy tales for babies.

I don't think that all the media that an adult consumes should be adult in
nature. The adults who play Zelda also consume other media geared towards
adults. Regardless I did have to get this off my chest.

The plot seems tired and beaten. It's Ganon for the umpteenth time and you have
to save Zelda again. At least that is what it looked like from what I was able
to see. There could be some twists brewing. There is always Ganon or Vaati or
some shit like that. I don't know how people are not sick of this shit already.

After I got to the second Temple, I got bored and felt like playing *Elden
Ring* instead.

## Why Elden Ring

*Elden Ring* is a video game software from From Software. It is produced by a
Japanese studio just like Zelda. But it is mature in its themes and has deeper
lore that reads like it was not written for infants. The narrative style is
similar to other Souls games. The worldbuilding has the same soft pitfalls that
are common in fantasy writing like the manic obsession with kings, queens,
bloodlines and so on. All the major players in the story are gods or their
descendants with god-like powers. I am willing to give this a pass because
their ontology explains it somewhat but it is still very straight-edge and
orthodox.

I love the lore in this game. Maybe because how many questions have been left
unanswered. I spent a lot of time watching lore videos (VaatiVidya and Zullie
the Witch primarily), talking to other people on a lore-oriented Discord server
and reading item descriptions. To me, Elden Ring's world feels like a religious
epic with all-powerful actors doing God knows what that you as a lowly
Tarnished cannot comprehend. There is a varied cast of Outer Gods, Gods and
Demi-Gods. Because of the inherent nature of the narration, the game does not
go into great details of how the world actually works so there is a sense of
mystique that is ever-present.

This would be my second playthrough of the game. The first one was on release.
Unfortunately I ended up ruining it out of sheer panic when I decided to rush
through the game by farming an egregious number of levels in the Moghwyn Palace
area and abusing the Mimic Tear spirit summon to cut through the bosses like a
hot knife through butter. This was mostly because of Margit, the Fell Omen and
I will expand more on this in the boss summaries section. But because of this,
after I had finished the game, I had a nagging feeling in the back of my brain
that I had cheated though I technically had not. But my fragile self-esteem
demands that I complete the game while making it reasonably difficult for
myself because I don't have much else going on for me. So being able to take
pride in being able to overcome these challenges helps in spite of however
fleeting and inconsequential it might be.

I had platinum'ed Sekiro a little while back which helped reign in my fear of
failure and reinforced faith in my skill in pressing buttons on a controller.
Remembering how much I enjoyed the lore of Elden Ring and having a feeling that
it might scratch that itch that Tears of the Kingdom caused but couldn't
satisfy, I decided to initiate my second playthrough of the game.

I set up some ground rules for myself to make the game reasonably difficult and
lay ground for a regret-free playthrough. I would:
- Use a melee-only build with medium load for the Dark Souls experience.
- Not use weapons with busted Ashes of War like Rivers of Blood. (Honestly I
  don't know how powerful it is exactly but I did not want to take chances.)
- Fight major bosses solo without resorting to help from Spirit Ashes, summons
  and so on. I would stick to these rules **within reason**. I may hate myself
  but there is a limit to it.

## Build

I started the Vagabond class. At first I used the Reduvia and the name-brand
flail. Then I moved on to the Lance Armstrong Flail and finally settled down on
the Bloodhound Fang. I remember finding it difficult to use powerstanced
greatswords in the first playthrough. The issue was related to swings being
slow and the bosses being Sanic. Since then larger swords had been buffed a
little bit. Bloodhound Fang's Ash of War is not too powerful against endgame
bosses. Either way I did not use it much. It seemed like a fair choice. Most
importantly, it is upgraded using Somber Smithing Stones so you don't have to
hoard Smithing Stones like a Tolkienesque dragon does with gold which saved me
a lot of headache. I also used the Buckler Shield for parrying Crucible
Knights.

I prioritised Vigor early on and then Strength and Dexterity to be able to use
my weapon of choice. I played most of the game with 15 Endurance and only
leveled it higher towards the end.

## Bosses

### Margit, the Fell Omen

Margit is first major boss fight in the game on the conventional route. I found
him extremely challenging with a melee only build which corroborates my memory
of him from the first playthrough. Back then, I tried to beat him with my melee
character and he instilled in me a fear of what horrors were to come as I
progressed and the difficulty ramped up. Riddled with anxiety, I took the "easy
way out" by exploiting legal game mechanics.

The problem I had is that he does not give any clear openings. He would trick
you into thinking that he just finished a combo, only to summon a knife in his
off-hand and take a swipe at you with it. This problem is exacerbated in the
second half where he gains the ability to do the same thing but now with a
sword. He can also now do combos that seem impossible to dodge by rolling.

I cheesed his second phase with Reduvia's Ash of War. I had earlier considered
playing the whole game with dual daggers but their powerstance attack felt
slow, the damage pathetic and the range abysmal. I just couldn't. However, the
Bleed build-up on the Ash of War is substantial and helped beat Margit.

Sometimes I try to imagine what I could do to beat him without cheesing. As I
progressed through the game, I learnt--albeit only a little bit--the importance
of spacing. Some fights become easier my putting and maintaining some space
between the enemy and you. With Margit, it would make it not necessary to try
and roll through his combos that are difficult to roll through. After that I
could wait for attacks like the overhead cane smash which give good openings.
It makes the fight longer but would give me a chance. Trying to waltz with him
in melee range seems like a losing strategy.

### Rennala, Queen of the Full Moon

I am not sure if she is supposed to be an example of the trope where
"emotional" women lose their heads because of separation from their male
partner or whether there was some magic at play and Marika tricked her into
lunacy.

Either way, this fight is trivial. Not much to say about this.

### Starscourge Radahn

As I was mentally preparing myself for the Radahn festival, I sensed a tinge of
fear somewhere within. The fight was difficult on the first playthrough. It
took me numerous tries. Plus Caelid is not a welcoming area. It makes you not
want to stay any longer than you have to.

Radahn had however been nerfed in one of the earlier patches but I wasn't sure
exactly to what extent. To my disappointment, the fight turned out to be
pathetically easy. Feeling festive, I used all the summons that the fight
provided assuming I would need them. He ended up dropping so fast he blew a
hole through the Mistwoods. Had I known he had so little HP I would have fought
him solo.

### Morgott, the Omen King

This fight is the pretty much the Margit fight on steroid. Mr. Omen brooks thee
no quarter in this encounter since he is the last person standing between you
and the Erdtree. I would have struggled with this fight a lot if my weapon, the
Bloodhound Fang, was not +9. I was chunking him for more than 400 damage per R1
swing which rendered the fight easier than I had expected. I think I died five
to ten times to him.

Had I known this would happen I would have not levelled up my weapon so much. I
think something between 250 and 300 damage per swing would be the sweet spot
for me.

He is relentless with his combos which demands melee players to stay away from
him to avoid being put in situations where the player has to roll through his
ridonkulous combos. Thank you for teaching me about spacing, Morgott.

### Rykard, Lord of Blashphemy

Praetor Rykard was an afterthought for me. Since he is optional and I
remembered the encounter to be a gimmick fight, I did not think of bothering
with him.

But now my perspective has turned. The fight is not a gimmick. The
Serpent-Hunter requirement is a fun twist but it does not make the fight
trivial. I struggled in this fight a fair bit. Part of it was that I was not
playing the fight optimally but instead I was consumed by a panic-driven urge
to rush him down. I tried to remain calm but I could not. Wonder why that is. I
did not struggle with him in the first playthrough. I wonder what changed.

Praetor Rykard and the God Devouring Serpent should have been given more
spotlight. I don't understand their Tarnished-hunting ways and how it serves
the ends of the Serpent god. I would have liked to learn more about how this
pantheon of weird outer gods interact with each other.

### Fire Giant

This fight was easier than I remembered from my previous experience. After
getting to recognise his quirks, the fight is a battle of attrition involving
staying away from him, swooping in when there are openings and whacking him,
all while being mounted on Torrent. This process has to be repeated until his
HP drops to zero.

On my first attempt, I got greedy and ended up dying with him having 1 HP left.
Insane.

After this fight, I decided to clean up the known demi-gods before proceeding
to Farum Azula.

### Mohg, Lord of Blood

Incredible fight.

Mohg does not have any bullshit mechanics other than his HP chunking curse
which heals him. Thankfully there exists an antidote for it. His combos are
very dodge-able but he punishes panic rolling **A LOT** as most of his swings
are heavily delayed. I found his design awesome to play against as a melee
build. You see, Mohg has been blessed by the Formless Mother and has become the
Lord of Blood. Now he is bursting at the seams with blood. My man is nuttin'
and busting' blood everywhere. This mechanic punishes you for staying away from
him since he will just throw bleed-inducing blood at you, a move that does not
have a punish. Your only option then is to get in his face, roll through his
combos and attack him in the openings between combos which are fairly wide.

The second phase is the first on steroid. There is more blood, new combos,
lower downtimes. He also flies.

The fight is extremely fair despite being soundly difficult. One of my
favourite fights of the game.

Zullie the Witch thinks that Mohg's model could have been planned for use as a
generic enemy which would have been disappointing since this fight is AWESOME.

### Malenia, Blade of Miquella

I have fond memories of this fight from my first playthrough. My character was
very high level (165). But because of soft caps my damage had plateaued. My
character was pure melee so I did not have any broken magic at my disposal. I
was tanky as I recall it did mean for much. I fought her SOLO and after a few
excruciating hours managed to defeat her SOLO. No Mimic Tear this time.

This time I got my ass handed to me. The fight itself is fairly difficult. She
has fast combos but low poise which is a decent trade-off. But she has a move
called the Waterfowl Dance which:
- Is extremely convoluted to dodge.
- One-shots you.

On the first playthrough, I used the Bloodhound Step Ash of War to dodge it. It
has been nerfed since then and is useless against this particular move. The
result is that if she starts this attack before I have a chance to get FAR
away, I am done. It was really bullshit. I decided it was not worth dealing
with this. I decided to summon a co-operator.

The co-operator was really skilled. I feel they could have solo'ed her but I
didn't want to be a deadweight. The fight turned out to be easy with their
help. I still got caught in the Waterfowl Dance twice, surviving with a sliver
of HP each time.

I am glad she is gone. Fuck the Waterfowl Dance. Whoever designed this move
owes me reparations.

With the Lands Between done and dusted, I skedaddle on to Farum Azula for a
fond rendezvous with my beautiful bois, मोटू-पतलू.

### Godskin Duo

This is the only mini-boss that I talk about here because I don't know who
thought this fight was good idea. These are two extremely aggressive and
powerful enemies in a small room. There are some pillars to use as cover but
they can demolish them. They have large AoE attacks and ranged attacks.

"Use sleep pots." Please fuck off. Jesus Christ.

I summoned Recusant Bernard for this fight and I am glad that I did because he
made the fight easy. TOGETHA, we beat them handily. Something tells me that we
are going to be friends for a long time.

This fight has some uncanny similarities with the Ornstein and Smough gank boss
battle. Both sport a मोटू-पतलू duo. Both stages have pillar arrangements to
presumably allow the player to create space for themselves. The difference is
that the Godskin Duo are input-reading bastards with long-ranged attacks and
the pillars in their stage are destructible.

### Gurranq, Beast Clergyman उर्फ़ Maliketh, the Black Blade

The first phase of this fight is extremely annoying. Gurranq does not stop
flailing. So you pretty much have to trade HP with him unless you are Ongbal.
Because of how annoying it is, I am out of patience by the time I reach the
second phase and end up bottling it.

The second phase gives Maliketh some spectacular attacks but they are
dodge-able with great ease. Most of the difficulty comes from the first phase
being an acute pain in the asshole.

I was also able to pull off the parry with the Blashphemous Claw, which is OP
by the way. Highly recommended.

I feel bad for Maliketh. As I understand, Marika had to betray him because
Maliketh's job was to not just protect Marika, but also make sure that she does
not violate the Golden Order. Maliketh does not get this though. He dies not
knowing why his loyalty was rewarded with betrayal.

A moment of silence for our boy.

### Godfrey, First Elden Lord उर्फ़ Hoarah Loux, Warrior

Another great fight. I melted this dude with the Mimic Tear in the before
times. Without my comrade, the fight posed a great challenge.

There is nothing tricky about dodging his combos but you have to stay on your
toes at all times. It felt fair as a melee character.

In the second phase he does not let you heal at all. I was scurrying around
like a rat trying find space and time to chug some estus but he would always
interrupt me. He is relentless.

I am still confused why he chooses to attack us. I swear I don't want to fight
half the people in this game but since it's From Soft there is no other option.

### Radagon of the Golden Order → Mr. Elden Beast

The existence of Radagon is still a mystery. He is a **champion** of the Golden
Order. But I am not clear on yet what a champion is in this context. There is
this whole thing about how Marika and him are the same entity but the nature of
this truth is shrouded in obscurity.

I liked this fight. It's great fun for melee-only builds. It was very difficult
especially considering you want to not use all of your estus and save some for
the following phase. Radagon has high poise, I was able to break his posture
rarely but he did not get hit-stunned at all. Not sure what happens with Acme
Inc. hammers. So the pattern of the fight involves dodging his combos and
sneaking in attacks in the small windows he exposes. What makes the fight
difficult is ability to teleport. When he teleports to you to close distance or
to just confuse you, it concludes with a small holy explosion which does
damage. I found it tough to react to it. It took me a long while to beat him
for the first time itself. After that I had to improve further to be able to
  vanquish him without wasting estus.

Upon defeat, Elden Beast appears. It fashions a sword out of Radagon's spine
and then haves at you. The first two thing that stands out about this fight
are:
- The Beast is YUGE. It is very large. This is uncommon in Elden Ring.
- The arena is YUGE. It is very large.

Elden Beast has at its disposal an arsenal consisting of holy-powered
projectile-projecting sword attacks, holy incantations and the occasional fire
regurgitation which I assume is also holy in essence somehow. The sword attacks
are simple to dodge. The incantations not so much so. He casts Elden Stars
which shoots a consistent barrage of homing projectiles for a long time. You
have no choice but to just keep running throughout its duration and
simultaneously dodge any sword attacks the Beast dishes out while the
incantation is in effect. The incantation that truly is unique is the one where
the Beast ascends, forms a holy damaging ring around your character which
begins to constrict as soon as it appears. The move to dodge this is to keep
running in any direction and jumping over the ring and continue running until
the ring has constricted completely exploded. The fight was easier than
Radagon's which I am thankful for.

I strongly feel the fight should have allowed you to use Torrent. I would loved
to fight the final boss of the main story alongside our spectral friend. That
would have required balancing the boss around Torrent's mobility but it would
have been fun.

With the beast defeated, we take our pick from the various endings afforded to
us (or we don't and let Marika hanging) and wait for the DLC to be released.
