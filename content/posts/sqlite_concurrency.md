+++
title = "SQLite as key-value store for concurrent Rust programs "
date = "2024-03-23"

[taxonomies]
Tags=["non-political"]
+++

Originally posted by [Byron](https://github.com/Byron) at [this GitHub discussion](https://github.com/the-lean-crate/criner/discussions/5).

---

The motivation for this article is to save you a day or two when making SQLite ready for your concurrent, async Rust program, using it as key-value store minimising the "SQL" part.

If you want to get to the advertised content, skip the 'Why…' sections.

### Why not Sled?

Maybe some of you may think why one would chose a 20 year old embedded SQL database written in "C" over [Sled][sled-lib-rs], a modern pure Rust key-value store!

The short answer is: Do try it, we need an alternative and every user makes it better. SQLite is not a key-value store, and it's not suitable for concurrent application out of the box. Sled can save you a lot of trouble, and it's "just working" in concurrent programs with an API that feels natural and powerful.

However, when I did try to use it (in a [version from 2020-02-27](https://github.com/spacejam/sled/tree/9b6b18ecc541f217f0d45d2ad3d910e754ffa08f)), I encountered a few issues which taken together made using sled too much of a risk given the requirements of my program.

* There may still be some time till v1.0.0 and  [one](https://github.com/spacejam/sled/issues/986) or the [other](https://github.com/spacejam/sled/issues/976) issue to encounter
* My database grew to 14.1 GB and the time to open it was dominated by IO, delaying application startup by seconds. In an earlier version that stored additional 16GB of assets, sled would read ~22GB on startup and write 7.5GB when opening the database.
* Migrating big sled databases (which may be required between releases) can take a lot of memory. In one instance when migrating a 27GB database, it would use 50GB of RAM - it was a miracle my now deceased MBP with 16GB of RAM managed to complete the job, and for the first time I was afraid to lose data that way.
* When storing about 210 thousand little marker objects that are nothing more than a key, accessing all of these in a random access manner caused the memory consumption to peak at 5GB, never going down below 3GB after that point despite the application being done.
* By default, the application's memory consumption would be around 300MB, and as it turned out most of it came from Sled. There are a few configuration options, but they didn't seem to allow me trading speed for memory to the extend I would have wanted.

Given that I would like to run my program on small hardware with no more than 512MB of RAM, the sometimes unexpectedly high use of memory made it a deal breaker, so I had to look elsewhere :/.

You, however, should really give it a try, as by the time you do, none of the above might still be an issue. Maybe nothing of the above would be an issue for you. I think Rust does need Sled, which right now is our best bet and I hope we will get there sooner than later.

### Why SQLite ?

A venerable 20+ year old embedded SQL database written in C doesn't seem like the go-to solution for a key-value store for concurrent applications.
And it isn't out of the box, but can be performing admirably when configured correctly.

On top of that, it's well-tested, proven and stable - nice traits for a database to store your valuable data.

The last paragraph from the [about-page](https://sqlite.org/about.html) reads…

> We the developers hope that you find SQLite useful and we entreat you to use it well: to make good and beautiful products that are fast, reliable, and simple to use. Seek forgiveness for yourself as you forgive others. And just as you have received SQLite for free, so also freely give, paying the debt forward.

…which made me fall in love with it a little.

From Rust one of the best ways to interact with it is [Rusqlite](https://lib.rs/crates/rusqlite), which puts a very rusty API on top of the C-interface SQLite exposes naturally. It's also the only option I tried, there was no other need.

Let's start from there!

### Configuring SQLite with concurrent applications

#### Many concurrent writers

By default, concurrent writes will lock the database on the first writer, and as writers don't queue up, all but the first one will fail.  There can be any amount of readers while there is no writer, which is exactly the rules that Rust enforces in your program.

A first step towards the solution is this chunk of SQL that wants to be at the start of your application

```sql
                PRAGMA journal_mode = WAL;          -- better write-concurrency
                PRAGMA synchronous = NORMAL;        -- fsync only in critical moments
                PRAGMA wal_autocheckpoint = 1000;   -- write WAL changes back every 1000 pages, for an in average 1MB WAL file. May affect readers if number is increased
                PRAGMA wal_checkpoint(TRUNCATE);    -- free some space by truncating possibly massive WAL files from the last run.
```

* **journal_mode = WAL**
  * This allows any amount of readers even in the presence of one or more writers. Writers obtain locks per table, allowing concurrent writes to different tables with automatic queuing for the actual writes. Multiple writers to the same table are not allowed. Transactions that include reads followed by writes will fail if a write happened in the mean time, which may have invalidated the read.
* **synchronous = NORMAL**
  * Only sync data to disk when absolutely necessary, at least at the end of each transaction.
  * This mode can still lead to bad write performance if there are many small transactions, and depending on your application, you might want to turn that off entirely and trigger syncs yourself.
* **wal_autocheckpoint = 1000**
  * This line makes an implicit value explicit, and means that every thousand write transactions it will write the changes in the WAL back to the actual database file, if possible. As this operation locks the whole database, by default it will fail if there are still readers around, which may cause the WAL file to grow quite large.
* **wal_checkpoint(TRUNCATE)**
  * On startup of the application, write all changes in the WAL file back to the database and truncate the WAL file to zero size. This helps to avoid unbounded growth and assures your database eventually gets to see the changes committed to the WAL file as well. Please note that keeping commits in the WAL file doesn't affect what readers see - the database appears consistent even though there are two files with data, instead of one.

Now most direct writes will work concurrently, but transactions who read before they write are still likely to fail in the presence of other writes performed while reading.

There are two measures to make this work. Firstly, there should be a busy handler which, in the simplest case, sleeps a little. It should be installed when opening a connection to the database.

```rust
    pub fn open_connection_no_async(&self) -> Result<rusqlite::Connection> {
        let connection = rusqlite::Connection::open(&self.sqlite_path)?;
        connection.busy_handler(Some(sleeper))?;
        Ok(connection)
    }
```

Secondly each transaction that plans to read before writing should begin in IMMEDIATE mode. Without it, it will first get a read lock for the read portion, and then try to upgrade to a write lock when needed. In my case, this didn't trigger the busy handler and would fail permanently.

```rust
    fn update(
        &self,
        progress: Option<&mut prodash::tree::Item>,
        key: impl AsRef<str>,
        f: impl Fn(Self::StorageItem) -> Self::StorageItem,
    ) -> Result<Self::StorageItem> {
        retry_on_db_busy(progress, || {
            let mut guard = self.connection().lock();
            let transaction =
                guard.transaction_with_behavior(rusqlite::TransactionBehavior::Immediate)?;
            let new_value = transaction
                .query_row(
                    &format!(
                        "SELECT data FROM {} WHERE key = '{}'",
                        Self::table_name(),
                        key.as_ref()
                    ),
                    NO_PARAMS,
                    |r| r.get::<_, Vec<u8>>(0),
                )
                .optional()?
                .map_or_else(
                    || f(Self::StorageItem::default()),
                    |d| f(d.as_slice().into()),
                );
            transaction.execute(
                &format!(
                    "REPLACE INTO {} (key, data) VALUES (?1, ?2)",
                    Self::table_name()
                ),
                params![key.as_ref(), rmp_serde::to_vec(&new_value)?],
            )?;
            transaction.commit()?;

            Ok(new_value)
        })
    }
```

```rust
connection.transaction_with_behavior(rusqlite::TransactionBehavior::Immediate)?
```

The above is the key to making this work. In conjunction with a busy handler that sleeps…

```rust
fn sleeper(attempts: i32) -> bool {
    log::warn!("SQLITE_BUSY, retrying after 250ms (attempt {})", attempts);
    std::thread::sleep(std::time::Duration::from_millis(250));
    true
}
```

…this call will block until a write lock was obtained before any read happened.

### Avoiding blocking busy handler for a better Future

In a threaded application, blocking the whole thread might be the way to go. With Futures, however, that is not advised in the common case as that may prevent all futures to make progress, depending on your executor.

Additionally using busy handlers per connection restricts the knowledge about what's going on within the handler, as these cannot be closures (i.e. they must be `fn(i32) -> bool`).

To solve this, one can refrain from installing a busy handler…

```rust
    pub fn open_connection(&self) -> Result<ThreadSafeConnection> {
        Ok(std::sync::Arc::new(parking_lot::Mutex::new(
            rusqlite::Connection::open(&self.sqlite_path)?,
        )))
    }
```

…and handle SQLite Busy failures by yourself:

```rust
    fn update(
        &self,
        progress: Option<&mut prodash::tree::Item>,
        key: impl AsRef<str>,
        f: impl Fn(Self::StorageItem) -> Self::StorageItem,
    ) -> Result<Self::StorageItem> {
        retry_on_db_busy(progress, || {
            let mut guard = self.connection().lock();
            let transaction =
                guard.transaction_with_behavior(rusqlite::TransactionBehavior::Immediate)?;
            // ...
```

The retry-utility itself can receive additional context to better integrate with your program.

```rust
fn retry_on_db_busy<T>(
    mut progress: Option<&mut prodash::tree::Item>,
    mut f: impl FnMut() -> Result<T>,
) -> Result<T> {
    use crate::Error;
    use rusqlite::ffi::Error as SqliteFFIError;
    use rusqlite::ffi::ErrorCode as SqliteFFIErrorCode;
    use rusqlite::Error as SqliteError;
    use std::ops::Add;

    let max_wait_ms = Duration::from_secs(100);
    let mut total_wait_time = Duration::default();
    let mut wait_for = Duration::from_millis(1);
    loop {
        match f() {
            Ok(v) => return Ok(v),
            Err(
                err
                @
                Error::Rusqlite(SqliteError::SqliteFailure(
                    SqliteFFIError {
                        code: SqliteFFIErrorCode::DatabaseBusy,
                        extended_code: _,
                    },
                    _,
                )),
            ) => {
                if total_wait_time >= max_wait_ms {
                    log::warn!(
                        "Giving up to wait for {:?} after {:?})",
                        err,
                        total_wait_time
                    );
                    return Err(err);
                }
                log::warn!(
                    "Waiting {:?} for {:?} (total wait time {:?})",
                    wait_for,
                    err,
                    total_wait_time
                );
                progress.as_mut().map(|p| {
                    p.blocked("wait for write lock", Some(SystemTime::now().add(wait_for)))
                });
                std::thread::sleep(wait_for);
                total_wait_time += wait_for;
                wait_for *= 2;
            }
            Err(err) => return Err(err),
        }
    }
}
```

**Please note** that even though I chose to block, it should be possible to do an async sleep instead of a blocking one. For my program, however, that wasn't required as tasks are distributed to threaded executors, where blocking futures will not prevent all other futures from making progress.

### Writing many small objects, fast

When writing many small objects intuitively (that is, naively unfortunately), performance will be low and dominated by IO as every write commits by default which triggers an fsync.

Something that may improve the situation at the expense of safety is to set [`synchronous = OFF`](https://sqlite.org/pragma.html#pragma_synchronous) and skip fsync() all together.

I took a different route and decided to go with prepared statements within a transaction.

```rust
                let transaction = {
                    store_progress.blocked("write lock for crates", None);
                    let mut t = connection
                        .transaction_with_behavior(rusqlite::TransactionBehavior::Immediate)?;
                    t.set_drop_behavior(rusqlite::DropBehavior::Commit);
                    t
                };
                {
                    let mut statement =
                        new_key_value_insertion(CrateTable::table_name(), &transaction)?;
                    store_progress.init(Some(crates_lut.len() as u32), Some("crates"));
                    for (cid, (key, value)) in crates_lut.into_iter().enumerate() {
                        statement.execute(params![key, rmp_serde::to_vec(&value)?])?;
                        store_progress.set((cid + 1) as u32);
                    }
                }
                store_progress.blocked("commit crates", None);
                transaction.commit()?;
```

Much of this code is boilerplate to communicate the application state to [`prodash`](https://lib.rs/crates/prodash), and it relies on using a connection that blocks while the we try to get an immediate-mode transaction. From there, a prepared statement is used to insert changing key-value pairs followed by a single commit in the end. This performs admirably and I have seen 220k objects inserted in less than a second.

### Gains and losses compared to using Sled

The sled version I used previously was a [version from 2020-02-27](https://github.com/spacejam/sled/tree/9b6b18ecc541f217f0d45d2ad3d910e754ffa08f). It must be clear to the reader that I am a big fan of Sled, and I can't wait to try it again. For this project, however, I wasn't able to and find it very unfortunate.

* **Gains**
  * predictable, low memory consumption
  * exhaustive documentation of expected runtime behaviour
  * connect to the same database from multiple processes
  * Easy introspection thanks to [sqlitebrowser](https://sqlitebrowser.org)
  * Database size about 4 times smaller (14.11GB down to 3.4GB, 3.1GB zipped down to 700MB)
  * No zero-copy
    * SQlite does not support this at all as values are copied out of the database. Owning everything makes Rust code easier to read due to the lack of lifetimes, but certainly costs you in terms of allocations and memcopies. In retrospect, I consider making use of zero-copy from the get-go a premature optimization - the added code complexity isn't worth it with the Rust of today.
* **Losses**
  * Zero-copy
    * Originally I intended to use Sled as backing store for [rust-messagepack](https://lib.rs/crates/rmp) encoded blobs which are deserialized into structs that use [a lot of Cow's](https://github.com/crates-io/criner/blob/022a882b57b52b13d25b30c27eed98e5f3e8ae11/criner/src/model.rs#L109-L126). It worked perfectly and was too fast for progress bars :D.
  * Easy-to-use concurrent-by-default API that is a pleasure to use and 'just works™️'. SQLite needs a lot of massaging and your app to embrace the SQLite API to support that. About a day was spent on figuring out how to get things back to where they were.
  * Now you have to study the manual and solve concurrency issue that weren't there before.
  * No-sql - having a native key-value store is a big bonus to me
  * No lifetime issues - as SQLite transactions keep their connection as reference, refactoring gets awkwardly hard where I simply fail to express the lifetimes necessary.

###  A feel for the achieved parallelism and performance…

[![asciicast](https://asciinema.org/a/308380.svg)](https://asciinema.org/a/308380)

[sled-lib-rs]: https://lib.rs/sled
