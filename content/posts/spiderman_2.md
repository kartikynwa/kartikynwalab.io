+++
title = "Spider-Man 2 (PS5) (2023)"
date = "2023-10-24"

[taxonomies]
Tags=["non-political"]
+++

# Spider-Man 2 (PS5) (2023)

I was really excited about this game which is a bit embarrassing since I
hate capeshit. Why was I excited? High-budget single-player
microtransaction-free games are hard to come by these days so I take
what I get. Plus, it would help justify owning a PS5. There aren't many
next-gen exclusives for this platform.

In hindsight, I had somewhat of a false memory of the first game and
remembered liking it more than I actually did. I did this with *Horizon:
Zero Dawn* too as I was waiting for the sequel. Not sure why though.
Probably because I am desperately seeking to plug this gaping hole in
the core of my weary soul with excellent video game experiences as the
prospect of achieving genuine contentment wears thin with each passing
day. Can't say that for certain though.

As I played *Spider-Man 2*, the memories of the flaws I perceived in its
predecessors came back.

## Story and Setting

There is a handy feature in the game that catches you up with the story
so far in the series. But even though I don't remember the plot at all I
could not be arsed. I dove straight into the game accepting what it
threw at me at face value.

There are some things I like about the whole setting. Playing as Miles
and Peter is nice since they sound like good lads. They have good
personalities and the same goes for the rest of the cast—Mary Jane,
Ganke, Hailey, Rio, Harry. They are all almost too sweet and perfect
with no glaring flaws except for what misfortune gods have tossed their
way. For example, Harry is plagued by an incurable disease and MJ,
despite being an excellent journalist and researcher, has to write
tabloid trash to be able to hold a job and pay her bills.

The game has some missions where they put spotlight on often ignored
cultures and peoples. For example, one mission has you control Hailey, a
deaf Black girl, where you experience her limitations as well as
talents. Another questline requires you to find items stolen from a
museum–instruments belonging to legendary minority musicians of New
York. This is commendable because it goes a step beyond what the AAA
video-game-verse does in the name of representation, that is just having
coloured main or side characters. Let's take a brief, superficial look
at real-life New York City first. Despite being obscenely wealthy, the
wealth distribution in the city of New York is extremely unequal. Their
police force, which has a budget larger than that of many small
countries' militaries, are racist stormtroopers. They were infamous for
their racist stop-and-frisk policies until the mid-2010s. Now they have
moved to subtler forms of racism. The city faces issues with widespread
destitution. "In August 2023, there were 86,510 homeless people,
including 29,721 homeless children, sleeping each night in New York
City’s main municipal shelter system. A total of 23,912 single adults
slept in shelters each night in August 2023."[\[source\]](https://www.coalitionforthehomeless.org/basic-facts-about-homelessness-new-york-city/) This is by design.
There is enough wealth in the city to feed and house every individual.
The problem is that not that of scarcity of resources, but rather a
political and economic one. Much like the rest of the country and most
of the world, the economy of New York City is one where the rich elites
siphon off the wealth created by the working people both at home and
abroad. Whatever misery the people of NYC face, it is often not because
of supernatural causes outside their control, but because the wealthy
feed off of their misery.

The game's setting, on the other hand, is peak liberal utopianism. Their
representation of NYC makes it look like nothing is fundamentally wrong
with NYC. Sure, there are minor petty crimes here and there, but NYC
innately has the capability to solve these problems if it was not for
those dastardly super villains and their psychotic and
evil-for-the-sake-of-evil villainy. The role of super heroes like the
Spider-Men in this context becomes that of exterminating super villains
as they pop up. Forces like the police are relegated to the background
and are largely meaningless. What would Spider-Man be doing in this
universe if super villains were absent? Probably broken windows
policing.

The issues of poverty and homelessness are acknowledged. Aunt May was a
crucial member of F.E.A.S.T., a homeless shelter that features
prominently. But the roots of these issues are ignored. Poverty is shown
as an ontological truth that just has to be fought back against with
sheer willpower and goodness of the heart. Similarly, Rio, Mile's
Mother, is a congresswoman which is a nod to the delusion of the
effectiveness of electoral means. In real life, the latest "salt of the
Earth" congressperson from NYC is Alexandria Ocasio-Cortez who is
currently tacitly supporting Mexico border camps and condemning Hamas
uprisings against the Israeli perpetrated genocide and
settler-colonialism. There was a weird bit where a homeless Black man
who commands a flock of pigeons asks you to find a new home for them
because, as you later find out, he is on the precipice of death. When
you return to him after having completed the task, you see an ambulance
taking him away confirming his death. I don't know what the message of
this mission was. It was just sad and bizarre. His homelessness was
ignored. Miles goes to a school for the gifted which is sad when
juxtaposed to the reality of public schools being relentlessly attacked
by ghouls like Bill Gates.

Apart from these, the plot is typical of a Spider-Man story where a
protagonist struggles to balance their personal and professional lives
with their super-hero duties. One thing I really liked was when the side
characters, consumed by the Symbiote, open their hearts about how they
detest playing second fiddle to Spider-Man. I would like feel this way
too in their shoes but it is left unsaid for the most part. I liked the
passing of the Spider-Man torch from Peter to Miles as well because
Miles is a nicer person than Peter and it was great to see Peter
acknowledge that. This is not to say that he had his own motives as
well. Given her circumstances, MJ was given a role of more than just a
plot device which was new for this franchise.

## Gameplay

The gameplay is a bit shallow for my liking. The traversal is the most
fun part for me. I really enjoy swinging, zipping and gliding through
the city. I don't say this to diminish the combat. Even though I was not
a fan of the combat, I thought it was okay. The traversal is just that
fun.

Swinging back to the combat, it felt very shallow. The main pain point
for me were the animations. They felt very janky. Bad dudes punching in
slow motion was so bad to see. I feel like the developers and animators
decided to rely too much on dodge/parry timing indicators which allowed
them to slack off on the clearness of the attack animations. I would
like draw comparison to Soulsborne games and Sekiro, but this game
requires you to mow down hordes of enemies which changes the mechanics
substantially.

The boss fights were especially disappointing. The problems of bad
animation persists. But the bosses have very limited movesets which,
though a negative, was a relief considering how it meant there were
fewer bad animations to deal with. They felt like a slog consisting of
pressing the same button combination ten or twenty times until the boss'
health bar had completely depleted.

Overall the combat was underwhelming. The game gives Miles and Peter new
abilities throughout the entirety. You get new abilities up until the
third- or fourth-last story mission. But I couldn't get excited about
these because of how underwhelming the combat was.

## Other Points

The game technically is great. If my memory serves right, I encountered
only two or three bugs that were noticeable. The game crashed once.
Somehow, fast travel is instantaneous. I don't know how they did it but
it is really cool. The limitation of the open-world-ness probably has a
part to play in it but it is impressive nonetheless.

The accessibility options are great too. I made use of them by skipping
the puzzle sections and widening the dodge/parry window.

Not sure why I decided to skip the puzzle sections. I think my mind is
giving up on me. Or maybe I was just impatient. I hope it was the
latter.

## Overall…

I really wished the combat was better. This is disappointing because
Insomniac is working on a Wolverine game as well and now I don't feel
hopeful about it.

Even then the game was enjoyable. I would recommend pirating the game or
getting it on sale.
