+++
title = "Wo Long: Fallen Dynasty"
date = "2023-03-07"

[taxonomies]
Tags=["non-political"]
+++

# Wo Long: Fallen Dynasty

I had been looking for this game to release for a long time. From watching the
trailers and playing the demos, I liked the parry-based combat. It is (I think)
inspired by Sekiro but is still fundamentally different from From Software's
implementation. I appreciate Team Ninja going their own way and have innovated
a great combat system.

I was not in the perfect state of mind to play it when the time came owing to
the strange situation on the job front and the complications caused by `an
occasion` being on the day before. Still I was able to churn through the game
without much problems. All is good.

## Stuff Other Than The Boss Battles

The game is set in "Romance of the Three Kingdoms" era China. I still don't
know what the novel is about but I am reluctant to learn history from stuff
like this where kings, warlords, etc. are romanticised. I decided to not look
into this. Over the course of following the game, I did not encounter any rabid
Sinophobia from the player base. Whatever the reasons behind that may be, I am
relieved.

The leveling system seems to have been simplified. Compared to Nioh there are
only five stats ("Phases") to put points into. While I welcome this, I still
don't know how to level up properly. Thankfully the game does not need
min-maxing. I leveled up Water primarily until 30. Then Fire 'til ~20. Then
changed my mind and decided to level up Wood 'til 30 then I put points in Water
or Fire depending on the mood. As you can till this looks far from ideal
especially considering I don't know what the caps are. But I didn't too weak or
squishy at any point. I think maxing Wood second helped me a lot since it gives
the highest flat HP. Respec'ing is free but I didn't bother experimenting
because I am not about that life.

World design is still level based as opposed to open-world. I don't like this
but it's something that can be lived with. Level design itself is fine but not
to my taste. I like exploration to some extent but I found some levels easy to
get lost in. The level with the first Lu Bu fight was especially annoying. From
the starting point, there are branching paths that need to be explored to
gather Fortitude Points. But it's confusing. Moreover, there are enemy balistas
which one-shot you. It was a terrible experience. The levels in general are
much easier when the stealthy approach is taken as it allows taking on enemies
one-by-one without getting piled on.

There is a mechanic called Morale. Not going into the details of how it works.
I thought it was a good mechanic. I felt rewarded for doing the thing where I
clear every enemy when I play a level for the first time. In terms of actual
impact, the Morale level matters a lot. I am pretty sure most bosses could one-shot
you at 0 Morale. So the system actually gives incentive to explore.

The story is weak and the storytelling is weaker. I don't know why they don't
put effort into that. It would really elevate the quality of the game but I
guess they were short on time and money.

I am not really interested in the loot system. I don't know why it's a thing.
I only tried to "optimise" my loadout towards the very end but it felt
low/no-impact. I feel NG+ and later is something that would benefit from this.
Early on it just gets in the way and is best ignored.

The combat feels nice. Mob goons in one-versus-ones are easy to rush down. In
numbers, things get tricky. I would say compared to Souls, Sekiro, etc. enemies
in numbers cause more problems in this game. Boss fights are fun. The game's
fundamentals lay down a good foundation for challenging but fair boss fights.
In terms of options there are: melee attacks, wizardry spells and weapon
martial arts. I did not experiment with the latter two too much but there is
good reason to use them. For wizardry spells, I used the ones which temporarily
give the wielded weapon elemental damage and the one which enhances stealth
temporarily. Martial arts vary in quality. Some are great and some are useless.
I found the ones with good mobility generally useful. Weapon variety is alright
I guess. I am not sure how to judge this aspect properly. For example, there
are a lot of sword variants---swords, sabres, podaos, etc. I am not sure how
different these actually are apart from the stat scaling. Their movesets felt
similar. But luckily, apart from swords there are spears, glaives, cudgels and
so on. So some variety is at least there. I played the whole game with a glass
cannon dual swords build. I did not want to overwhelm my smol brain with extra
stuff to factor in.

The Spirit meter is the heart of the combat. Spirit is similar to posture in
Sekiro but also quite different since it is the resource used to cast spells
and martial arts. Blocking attacks costs Spirit and deflecting them builds it
up. The heavy attack also costs spirit. It actually eats up all the accumulated
spirit. The Spirit it consumes, the more damage it does---HP and Spirit. For
boss fights, I found that the key loop involves using deflects and light
attacks to build up spirit. Once it is maxed out, use the heavy attack to do
spirit damage. I watched an Ongbal clip and he plays differently. He uses
spirit for martial arts instead. Both approaches seem fine. The issue is that I
think new players will have a difficult time figuring this out. In Sekiro,
through the tutorial prompts, the narrower focus of posture and the extreme
importance of deathblows, it is easier to figure out the "correct way". Here,
I feel I was able to find my way only because I had played Sekiro before. I can
see new players fall into the trap of focusing too much on doing HP damage with
light melee attacks.

Overall I loved the combat. I think with my stealthy glass cannon build I chose
the most fun playstyle for me.

## Boss Battles

I am going to go through some of the bosses as I encountered them and share my
brief thoughts on the design.

### Zhang Liang, General of Man

This is by far the toughest first boss in any game as far as I know. Should the
first boss be this difficult? I think no. But because I had beaten him
countless time in the demos, I had no issues. Design-wise I think they should
straight up have made the second phase easier. Apart from this, the boss does a
good job at teaching the important of Spirit-breaking into fatal attacks over
wearing out the HP slowly.

### Zhang Bao, General of Earth

I actually don't remember this boss very well. I wasn't able to figure him out.
Took a few tries to take him. He is a normal sized human but because of his
mobility he cannot be rushed down like Zhang Liang's first phase.

### Zhang Jiao, General of Heaven

Same as above. I don't remember much. I beat him in the first or second try. As
with the previous boss, I had allies with me. So I didn't understand him too
well. I was just eager to get past him.

### Aoye

Had a lot of trouble with this one. It does way too much damage with the
perilous attacks and the timing for them is weird. It's more rhythm based than
reactive. The charging red attack one-shot me. Shit was tough.

### Baishe

Snek lady gave me a little bit of trouble. I wasn't able to figure her out too.
Her red attacks were tough to deflect.

### Zhang Rang

He is called an Eunuch for some reason I don't want to look into. This fight
was troll as hell. It was super annoying but I surprised myself by taking it
well.

The guy spawns about ten clones of himself and they have to be taken out before
trying to fight his actual self. This was the super annoying part but I enjoyed
trying to figure out how to do this given that the game is not conducive to
large AOE mob-clearing moves.

Overall, the fight was a good bit.

### Hua Xiong

Fuck this bastard. He guards the gate to Lu Bu's boss fight. Maybe if 18 morale
before fighting him, the fight would have been easier. But this level as a
whole was a fuck. I wanted to get over with it ASAP. So I just beat my head up
against him until I won. I feel if I fought him now patiently I would be able
to understand him a lot better.

### Lu Bu

This fight was brilliant. Lu Bu is supposed to be a legendary warrior and the
character was definitely awe-inspiring.

The fight begins with Lu Bu mounted on Red Hare. The aim here is to break his
posture which is best done by letting him come to you, deflect the red attacks
and throw in a heavy attack whenever possible.

On foot, he swings around his massive glaive. Deflection is key here along with
heavy attacks sprinkled in.

The fight was tough but fair. Really enjoyed it.

### Taotie

This was the first larger-than-life sized boss. Pretty easy though which I am
glad about because the level was a fuck.

### Sun Jian

This was a neat boss as well. Deflections and heavy attacks being key. Beat him
first time but that was because I had allies. Had to fight him again in a
sub-mission after which I understood him a lot better. Good fight.

### Dong Zhuo

This took me many tries. Was a good fight with a moveset that suits the guy.
While I was getting my ass kicked, I thought that his moves looked deceptively
easy to read and expecting a second health bar which honestly distracted me a
little. Thankfully no ruses of that sort were played that day.

### Xiahou Dun

I remember he flew around a lot. I had allies so I wasn't able to get a good
read on this boss. Surprisingly I never saw him again. So not much to say here.

### Zhang Liao

This fight was great. His moveset includes fast combos which are meant to be
deflected to build up Spirit, which is then supposed to be used with a heavy
attack to break his Spirit. The moveset is really unique and I had to spend
some time trying to figure him out. Thankfully I had another change to fight
him in a sub-mission so my win against him was not a fluke. Very well designed
boss.

### Lu Bu (Demon Form)

This was easier than the first Lu Bu fight. The moves are better telegraphed so
are easier to deflect.

### Yan Liang & Wen Chou

Motu-Patlu gank squad. I saw that the larger one can revive the twink but not
sure if vice versa is possible too. The fight was easy with allies. I would not
want to fight it solo though.

### Liu Bei (Demon Form)

I have no memory of this fight. Again, it's because I had allies with me.

### Yuan Shao (Demon Form)

The fight is super easy but the demon was pretty interesting. The demon feels
like a mech. It has a large shield which shoots ice wizardry and propels him
backwards. I have beaten with twice without breaking a sweat.

### Yu Ji & The Embodiment of Demonic Qi

Yu Ji, the bastard Taoist is the first phase. Pretty easy I would say.

The Embodiment of Demonic Qi was trickier but mostly because its red attacks
are very confusing to deflect. So it was tough but for the wrong reasons.

### Blindfolded Boy

So I would have never guessed that the Blindfolded Twink would be the final
boss.

His moveset is similar to Yu Ji's but he does a lot more damage. Was a tough
fight and took me a lot of tries. It was fun though.

### Honourable Mention: The Tiger's Loyal Subjects

This a sub-misison where you fight three NPCs at the same time: Cheng Pu, Han
Dang, and Huang Gai.

This was the most troll fight in the game. Super annoying. The key without
cheesing seemed to be baiting red attacks, deflecting them and then performing
fatal attacks when there was a chance to. So there was a lot of patience and
RNG involved.

I am glad it was put in the game though because reading people raging about it
online was worth it.

## Screenie

![A screenshot of my character in the game Wo Long: Fallen Dynasty](/wo_long_screenshot.jpg) 
