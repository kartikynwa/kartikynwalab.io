+++
title = "Optimising SQLite for Servers"
date = "2024-04-01"

[taxonomies]
Tags=["non-political"]
+++

Originally posted by [Sylvain Kerkour](https://kerkour.com) at [Optimizing SQLite for servers](https://kerkour.com/sqlite-for-servers).

---

# Optimizing SQLite for servers

*February 15, 2024*

SQLite is often misconceived as a "toy database", only good for mobile
applications and embedded systems because it's default configuration is
optimized for embedded use cases, so most people trying it will
encounter poor performances and the dreaded `SQLITE_BUSY` error.

But what if I told you that by tuning a few knobs, you can configure
SQLite to reach \~**8,300 writes / s** and \~**168,000 read / s**
concurrently, with 0 errors, on a \~40€ / m commodity virtual server
with 4 vCPUs (details and code in the [appendix](#appendix)).

Let's say that your server application is making in average 8 database
read queries per request, you could, in theory, handle \~21,000 requests
per seconds, or \~1,814,300,000 requests per day, for \~40€ per month,
bandwidth included, not bad! (In practice you may not be able to do
that: the server's bandwidth will be the limiting factor)

And this is before talking about tuning the garbage collector, [caching
and CDNs](https://kerkour.com/cloudflare-for-speed-and-security).

For more number crunching, take a look at Expensify's article: [Scaling
SQLite to 4M QPS on a single
server](https://blog.expensify.com/2018/01/08/scaling-sqlite-to-4m-qps-on-a-single-server)
in 2018.

**TL;DR?**

```sql
    PRAGMA journal_mode = WAL;
    PRAGMA busy_timeout = 5000;
    PRAGMA synchronous = NORMAL;
    PRAGMA cache_size = 1000000000;
    PRAGMA foreign_keys = true;
    PRAGMA temp_store = memory;
```

Use `BEGIN IMMEDIATE` transactions.

```go
writeDB.SetMaxOpenConns(1)
readDB.SetMaxOpenConns(max(4, runtime.NumCPU()))
```

Use `STRICT` tables.

## The biggest problem with SQLite: The `SQLITE_BUSY` error

`SQLITE_BUSY` (or `database is locked`, depending on your programming
language) is certainly the error that you will encounter the most often
when starting to use SQLite with servers, so it's important to
understand its meaning and why it happens.

SQLite currently allows only 1 writer to the database at a time. To do
that, the SQLite engine locks the database for writes.

Thus, the meaning of `SQLITE_BUSY` is simple: "I tried to obtain a lock
on the database but failed because a separate connection/process held a
conflicting lock".

If different threads / processes try to write to the database at the
same time, SQLite will wait a little bit and retry. After `busy_timeout`
(more on that later) milliseconds of retries, SQLite returns the
`SQLITE_BUSY` error.

## Optimizing SQLite for servers

Enough talk, let's dig into the code!

SQLite is, by default, optimized for client applications such as mobile
apps and embedded devices, but with the right settings, we can make it
great for servers too.

### Configuring the right PRAGMAs

[`PRAGMA` commands](https://www.sqlite.org/pragma.html) should be issued
to the database just after openning the connection, with a simple
`db.Exec` call or similar.

```sql
PRAGMA journal_mode = WAL;
```

The `WAL` journal mode provides a [Write-Ahead
Log](https://www.sqlite.org/wal.html) provides more concurrency as
readers do not block writers and a writer does not block readers,
contrary to the default mode where readers block writers and vice versa.

```sql
PRAGMA synchronous = NORMAL;
```

"When synchronous is NORMAL, the SQLite database engine will still sync
at the most critical moments, but less often than in FULL mode. WAL mode
is safe from corruption with synchronous=NORMAL".

It provides the best performance with the WAL mode.

```sql
PRAGMA cache_size = 1000000000;
```

Increase the SQLite cache.

When you change the cache size using the `cache_size` pragma, the change
only endures for the current session. The cache size reverts to the
default value when the database is closed and reopened.

    PRAGMA foreign_keys = true;

By default, for historical reasons, SQLite does not enforce foreign key,
you need to manually enable them.

    PRAGMA busy_timeout = 5000;

As we saw previously, setting a bigger `busy_timeout` helps to prevent
`SQLITE_BUSY` errors.

I like to use 5000 (5 seconds) for user-facing applications such as an
API and 15000 (15 seconds) or more for backends applications such as a
queue or machine-to-machine API.

### Use IMMEDIATE transactions

This one may be one of the biggest footguns of SQLite.

By default, SQLite starts transactions in `DEFERRED` mode: they are
considered read only. They are upgraded to a write transaction that
requires a database lock in-flight, when query containing a
write/update/delete statement is issued.

The problem is that by upgrading a transaction after it has started,
SQLite will immediately return a `SQLITE_BUSY` error without respecting
the `busy_timeout` previously mentioned, if the database is already
locked by another connection.

This is why you should start your transactions with `BEGIN IMMEDIATE`
instead of only `BEGIN`. If the database is locked when the transaction
starts, SQLite will respect `busy_timeout`.

It can be configured at the pool level [in
Go](https://github.com/mattn/go-sqlite3) with
`mydb.db?_txlock=immediate` and with `"transaction_mode": "IMMEDIATE"`
in
[Django](https://github.com/django/django/commit/a0204ac183ad6bca71707676d994d5888cf966aa)
versions 5.1+

### 1 Write connection, multiple Read connections

Finally, the last step to completely eradicate `SQLITE_BUSY` errors, is
to use only 1 connection for your write queries and guard it behind a
mutex. In Go it can be achieved with `db.SetMaxOpenConns(1)`. Thus, the
write lock will be managed by a mutex and the writes queued on the
application side, instead of relying on SQLite built-in retry and
`busy_timeout` mechanism.

If we stopped here, we would have bad read performance because our read
queries would compete with writes queries for our single database
connection, but as we saw, with the `WAL` journal mode, SQLite allows
for infinite readers even when the ddatabase is locked for writes.

The trick is to have 2 database connection pools: one pool for writes
with `SetMaxOpenConns(1)` and 1 pool for reads that scales with you
number of CPUs.

```go
writeDB, err := sql.Open("sqlite3", connectionUrl)
if err != nil {
    // ...
}

writeDB.SetMaxOpenConns(1)

readDB, err := sql.Open("sqlite3", connectionUrl)
if err != nil {
    // ...
}
readDB.SetMaxOpenConns(max(4, runtime.NumCPU()))
```

I personally hide this complexity behind a cusotm `DB` struct where its
`Select`/`Get` methods use the `ReadDB` connection pool and
`Exec`/`ExecSelect`/`Transaction` methods use the `WriteDB` connection
pool.

```go
type DB struct {
    writeDB *sqlx.DB
    readDB  *sqlx.DB
}

func (db *DB) Exec(ctx context.Context, query string, args ...any) (sql.Result, error) {
    return db.writeDB.ExecContext(ctx, query, args...)
}

func (db *DB) Select(ctx context.Context, dest any, query string, args ...any) error {
    return db.readDB.SelectContext(ctx, dest, query, args...)
}

// ...
```

### Batch your writes in a single transaction

Where you application allows it, batch your writes into a single
transaction.

```go
err = Db.Transaction(func (tx *Tx) (err error) {
    for _, event := range events {
        err = tx.Exec("...", ...)
        if err != nil {
            return err
        }
        return nil
    }
})
```

SQLite can easily reach \~250,000+ writes / s on the commodity server
mentionned above when batching inserts.

### Sharding

When you reach SQLite's limits in terms of writes performance for your
hardware, it's a good idea to split your database into multiple, smaller
pieces.

For example, the server application I'm currently working on uses 3
databases:

-   `myservice.db` which is the main database containing all the users
    and business data. \~75% reads / 25% writes.
-   `myservice_queue.db` which is a queue used for background jobs.
    \~98% writes / 2% reads, as even pulling jobs from the queue require
    an `UPDATE` query.
-   `myservice_events.db` is where I store all the events ([there's
    always an events table](https://brandur.org/fragments/events)).
    \~90% writes / 10% reads.

The events database is the most write intensive one, so I batch events
insertion within a single transaction every 20 milliseconds or every
time that the in-memory events buffer reaches 10,000 records.

### Use `STRICT` tables

By default, SQLite is "weakly typed": it will not complain and totally
accept inserting a string into an `INT` column.

This is bad.

Since version 3.37, SQLite supports a [`STRICT`
mode](https://www.sqlite.org/stricttables.html) for tables and will
enforce strong typing.

```sql
CREATE TABLE example (
    id BLOB NOT NULL PRIMARY KEY,
    created_at INTEGER NOT NULL,
    something INT NOT NULL
) STRICT;
```

## Operations and Backups

### Litestream

[Litestream](https://litestream.io/) is **THE** secret weapon that makes
using SQLite not only possible, but also a great idea.

Litestream provides "Streaming replication for SQLite". What does it
means?

Litestream is a daemon which replicates you database's WAL in the
background and stores it on an S3-compatible server. It enables (async)
point-in-time recovery and live backups for your SQLite databases and,
in case of outage, lets you restore your database without losses. All
that for only a few cents per month of S3 costs.

![Litestream live
replication](/sql_for_servers/sqlite_litestream.png)

You can read [here](https://litestream.io/how-it-works) how it works in
details.

### Filesystem

The SQLite authors recommend against using [SQLite on a networked
filesystem](https://www.sqlite.org/useovernet.html), as some networked
filesystem have faulty lock mechanisms which may lead to the corruption
of the database if it's accessed by multiple processes.

But this scenario can happen only if multiple processes/machines
concurrently access the multi-machines networked filesystem (NFS). On a
machine with a single-machine filesystem but connected to a network
volume, such as AWS EBS or Scaleway Block Storage, the kernel takes care
of the performances and correct locking of the database.

So you have two options here.

Either you use SQLite on a bare metal server, with multiple disks in
RAID configuration for reliability and availability.

Or, if you use SQLite in the cloud, I recommend to put your SQLite
database on a single-machine filesystem (ext4) on a networked volume
that is accessible by only 1 machine, such as AWS EBS or Scaleway Block
Storage, with enough IOPS. These networked volumes provide greater
reliability and availability than local SSDs, as disks are certainly the
pieces that fail the most often for servers.

Note that since 2017, [AWS EFS networked filesystem supports NFS version
4
lock](https://aws.amazon.com/about-aws/whats-new/2017/03/amazon-elastic-file-system-amazon-efs-now-supports-nfsv4-lock-upgrading-and-downgrading)
which allows SQLite to be safely accessed by multiple servers, but
networked locks offer rather poor performances (think milliseconds locks
instead of microseconds) and should only be considered if you have
absolutely no other choice.

Also, in theory you can use EFS with [lambda serverless
functions](https://aws.amazon.com/blogs/compute/using-amazon-efs-for-aws-lambda-in-your-serverless-applications).
To do that, you have two sane options.

First, if your database is read-only and updated very unfrequently
(every month or so), you can [bake
it](https://simonwillison.net/2021/Jul/28/baked-data) with your code
into your `lambda.zip` bundle.

Or, if your databae is read-only, but updated frequently (every hour /
day), [the other
solution](https://paulbradley.dev/sqlite-efs-serverless-database) is to
put the primary database on an EFS volume and copy it into your lambda's
`/tmp` directory on startup to get far better performances (microseconds
instead of milliseconds),

![Lamba functions accessing an EFS volume and copying an SQLite database
to
/tmp](/sql_for_servers/sqlite_lambda_efs.png)

### Zero-downtime application updates

Use the
[`SO_REUSEPORT`](https://stackoverflow.com/questions/14388706/how-do-so-reuseaddr-and-so-reuseport-differ/14388707)
socket option so you can:

-   Start the new version of you app on the same port as the old one
-   Stop the old application

### Zero-downtime server upgrades

Contrary to popular belief, it's totally possible to upgrade the machine
hosting the database without downtime, with the help of a reverse proxy
which will hold connections the few seconds that we detach the storage
volume from the old server, and attach it to the new one.

1.  Configure the reverse proxy to forward the requests to old and new
    server
2.  Stop the web application on the old server
3.  Unmount and detach the storage volume from the old server
4.  Attach and mount the storage volume to the new server
5.  Start the web application on the new server
6.  Stop the old server

All of that in less than 5 seconds, with the reverse proxy holding the
incoming connections.

![Zero-downtime server
update](/sql_for_servers/sqlite_server_upgrade.png)

### What about zero-downtime failovers?

But what about the situations where your machine unexpectedly dies or
your [datacenter
burn](https://www.bleepingcomputer.com/news/technology/ovh-data-center-burns-down-knocking-major-sites-offline)?

This, is the only part where things become tricky.

That being said, it's not that hard to implement a less-than-5-minutes
downtime failover script.

For that you need 2 servers in 2 different datacenters, and ideally at 2
different providers.

When your monitoring system detect that you primary server is down for X
minutes:

-   They pull the latest database on the failover server which is in
    another datacenter
-   Start the web app on the failover server
-   If needed, update the DNS records to point to the failover server

And that's all.

Now let's run the numbers: to pretend 99.99% availability, you are
allowed 52.60 minutes of dowtime per year. If you failover takes 5
minutes to switch, you could have 10 machines to die per year (!) and
still have you 99.99% label.

The reality is that modern machines, when administred correctly, are far
more reliable than that, and you could easily reach 99.999% ("five
nines") reliability, granted that you don't push any broken releases.

Anyway, the simplicity of SQLite will prevent you so [many
outages](https://k8s.af/) that I think that 5 minutes failovers is a
small risk when you compare to the kafkaesque architecture that most
applications are running on today.

## Future concurrency improvements?

There are 3 projects in progress to make concurrent writes faster with
SQLite:

-   [`BEGIN CONCURRENT`](https://www.sqlite.org/cgi/src/doc/begin-concurrent/doc/begin_concurrent.md)
    which locks only the affected pages instead of the whole database
    for writes.
-   [`WAL2`](https://sqlite.org/cgi/src/doc/wal2/doc/wal2.md) which
    improves the WAL mode.
-   And the [HCTree
    project](https://sqlite.org/hctree/doc/hctree/doc/hctree/index.html)
    which enables insanely concurrent writes.

It's not clear if or when they will be merged into the main branch. As
every developer knows concurrency is hard, and the SQLite authors have
to be sure that these features won't introduce any data corruption
issues.

## SQLite pitfalls

### Updating the schema can be slow

While you can `ALTER TABLE DROP COLUMN` since [SQLite
3.35](https://www.sqlite.org/changes.html#version_3_35_0), it's
generally slow when your tables are large.

### No TIMESTAMP type

The lack of a `TIMESTAMP` type means that you need to use either Unix
timestamps as `INT` or ISO-8601/RFC-3339 timestamps as `TEXT`.

I personally use Unix millisecond timestamps with a custom `Time` type
which offers the best performance.

```go
type Time int64

func (t *Time) Scan(val any) (err error) {
    switch v := val.(type) {
    case int64:
        *t = Time(v)
        return nil
    default:
        return fmt.Errorf("Time.Scan: Unsupported type: %T", v)
    }
}

func (t *Time) Value() (driver.Value, error) {
    return *t, nil
}
```

### COUNT queries are slow

SQLite doesn't keep statistics about its indexes, unlike PostgreSQL, so
`COUNT` queries are slow, even when using a `WHERE` clause on an indexed
field: SQLite has to scan for all the matching records.

One solution is to use a
[trigger](https://stackoverflow.com/questions/8988915/sqlite-count-slow-on-big-tables)
on `INSERT` and `DELETE` that updates a running count in a separate
table then query that separate table to find the latest count.

## Distributed SQLite

> **Update March 2024: [Distributed SQLite: Paradigm shift or
> hype?](https://kerkour.com/distributed-sqlite)**

Before we conclude, I wanted to talk a little bit about the exciting
projects trying to turn SQLite into a distributed database.

The underlying idea is roughly the same for all these projects: have a
single database for writes, and replicates it to an "infinite" number of
read-only replicas.

It allows not only to scale the reads, but, maybe more importantly, to
distribute your read replicas all around the world.

You have your main database in a single region, and it's replicated at
the edge for ultra fast reads. Think a worldwide response time of 50 ms
for your web app.

![LiteFS primary database / read
replicas](/sql_for_servers/sqlite_litefs.png)

-   The [D1 database from
    Cloudflare](https://kerkour.com/cloudflare-for-speed-and-security)
-   [https://github.com/superfly/litefs](https://github.com/superfly/litefs)
-   [https://github.com/rqlite/rqlite](https://github.com/rqlite/rqlite)
-   [https://github.com/losfair/mvsqlite](https://github.com/losfair/mvsqlite)
-   [https://github.com/chiselstrike/chiselstore](https://github.com/chiselstrike/chiselstore)
-   [https://github.com/canonical/dqlite](https://github.com/canonical/dqlite)
-   [https://github.com/vlcn-io/cr-sqlite](https://github.com/vlcn-io/cr-sqlite)

## Conclusion

In 2022, Ben Johnson declared: [I'm All-In on Server-Side
SQLite](https://fly.io/blog/all-in-on-sqlite-litestream). In 2024 me
too!

I will keep updated on how well it performs for real-world production
loads in a few months :)

## Appendix

### Benchmark results

All the results below were performed on disks encrypted with
`cryptsetup`:

    $ cryptsetup luksFormat --type luks2 --cipher aes-xts-plain64 --hash sha512 --iter-time 3000 --key-size 256 --pbkdf argon2id /dev/sdb

[Instances
datasheet](https://www.scaleway.com/en/docs/compute/instances/reference-content/instances-datasheet).

Scaleway `Stardust1-s` (1 shared vCPU, 1 GB RAM) with a 15K IOPS block
storage volume:

    elapsed 10.027475918s
    ----------------------
    272811 reads
    27206.348061 reads/s
    ----------------------
    20278 writes
    2022.243700 writes/s

Scaleway `PLAY2-MICRO` (4 shared vCPU, 8 GB RAM) with a 15K IOPS block
storage volume:

    elapsed 10.023892746s
    ----------------------
    1681936 reads
    167792.796932 reads/s
    ----------------------
    82953 writes
    8275.527492 writes/s

Scaleway `POP2-2C-8G` (2 dedicated vCPUs, 8 GB RAM) with a 15K IOPS
block storage volume:

    elapsed: 10.006010993s
    ----------------------
    1000497 reads
    99989.596324 reads/s
    ----------------------
    69477 writes
    6943.526251 writes/s

### Benchmark Code

Go version: `1.22.0`

Usage:

    $ mkdir sql && cd sqlitebench
    $ go mod init sqlitebench
    # copy the file below into main.go
    $ go mod tidy
    $ go run main.go

```go
package main

import (
    "database/sql"
    "database/sql/driver"
    "fmt"
    "log"
    "net/url"
    "os"
    "runtime"
    "sync"
    "sync/atomic"
    "time"

    "github.com/google/uuid"
    _ "github.com/mattn/go-sqlite3"
)

// Time is used to store timestamps as INT in SQLite
type Time int64

func (t *Time) Scan(val any) (err error) {
    switch v := val.(type) {
    case int64:
        *t = Time(v)
        return nil
    case string:
        tt, err := time.Parse(time.RFC3339, v)
        if err != nil {
            return err
        }
        *t = Time(tt.UnixMilli())
        return nil
    default:
        return fmt.Errorf("Time.Scan: Unsupported type: %T", v)
    }
}

func (t *Time) Value() (driver.Value, error) {
    return *t, nil
}

type entity struct {
    ID        uuid.UUID
    Timestamp Time
    Counter   int64
}

func setupSqlite(db *sql.DB) (err error) {
    pragmas := []string{
        // "journal_mode = WAL",
      // "busy_timeout = 5000",
      // "synchronous = NORMAL",
      // "cache_size = 1000000000", // 1GB
      // "foreign_keys = true",
      "temp_store = memory",
        // "mmap_size = 3000000000",
  }

    for _, pragma := range pragmas {
        _, err = db.Exec("PRAGMA " + pragma)
        if err != nil {
            return
        }
    }

    return nil
}

func main() {
    cleanup()
    defer cleanup()

    uuid.EnableRandPool()

    connectionUrlParams := make(url.Values)
    connectionUrlParams.Add("_txlock", "immediate")
    connectionUrlParams.Add("_journal_mode", "WAL")
    connectionUrlParams.Add("_busy_timeout", "5000")
    connectionUrlParams.Add("_synchronous", "NORMAL")
    connectionUrlParams.Add("_cache_size", "1000000000")
    connectionUrlParams.Add("_foreign_keys", "true")
    connectionUrl := "file:test.db?" + connectionUrlParams.Encode()

    writeDB, err := sql.Open("sqlite3", connectionUrl)
    if err != nil {
        log.Fatal(err)
    }
    defer writeDB.Close()
    writeDB.SetMaxOpenConns(1)
    err = setupSqlite(writeDB)
    if err != nil {
        log.Fatal(err)
    }

    readDB, err := sql.Open("sqlite3", connectionUrl)
    if err != nil {
        log.Fatal(err)
    }
    defer readDB.Close()
    readDB.SetMaxOpenConns(max(4, runtime.NumCPU()))
    err = setupSqlite(readDB)
    if err != nil {
        log.Fatal(err)
    }

    _, err = writeDB.Exec(`CREATE TABLE test (
     id BLOB NOT NULL PRIMARY KEY,
     timestamp INTEGER NOT NULL,
     counter INT NOT NULL
 ) STRICT`)
    if err != nil {
        log.Fatal(err)
    }

    log.Println("Inserting 5,000,000 rows")
    err = setupDB(writeDB)
    if err != nil {
        log.Fatal(err)
    }

    var recordIdToFind uuid.UUID
    row := readDB.QueryRow("SELECT id FROM test ORDER BY id DESC LIMIT 1")
    if row.Err() != nil {
        log.Fatal(row.Err())
    }
    row.Scan(&recordIdToFind)

    log.Println("Starting benchmark")

    concurrentReaders := 500
    concurrentWriters := 1
    var wg sync.WaitGroup
    var reads atomic.Int64
    var writes atomic.Int64
    ticker := time.NewTicker(10 * time.Second)
    start := time.Now()

    wg.Add(concurrentReaders)
    for c := 0; c < concurrentReaders; c += 1 {
        go func() {
            // we use a goroutine-local counter to avoid the performance impact of updating a shared atomic counter
          var readsLocal int64

            for {
                var record entity

                if len(ticker.C) > 0 {
                    break
                }

                row := readDB.QueryRow("SELECT * FROM test WHERE id = ?", recordIdToFind)
                if row.Err() != nil {
                    log.Fatal(row.Err())
                }

                row.Scan(&record.ID, &record.Timestamp, &record.Counter)
                readsLocal += 1
            }
            reads.Add(readsLocal)
            wg.Done()
        }()
    }

    wg.Add(concurrentWriters)
    for c := 0; c < concurrentWriters; c += 1 {
        go func() {
            timestamp := start.UnixMilli()
            // we use a goroutine-local counter to avoid the performance impact of updating a shared atomic counter
          var writesLocal int64

            for {
                if len(ticker.C) > 0 {
                    break
                }

                recordID := uuid.Must(uuid.NewV7())

                _, err = writeDB.Exec(`INSERT INTO test
                 (id, timestamp, counter) VALUES (?, ?, ?)`, recordID[:], timestamp, writesLocal)
                if err != nil {
                    log.Fatal(err)
                }
                writesLocal += 1
            }
            writes.Add(writesLocal)
            wg.Done()
        }()
    }

    wg.Wait()

    elapsed := time.Since(start)

    log.Println("Benchmark stopped:", elapsed)
    fmt.Println("----------------------")

    log.Printf("%d reads\n", reads.Load())

    throughputRead := float64(reads.Load()) / float64(elapsed.Seconds())
    log.Printf("%f reads/s\n", throughputRead)

    fmt.Println("----------------------")

    log.Printf("%d writes\n", writes.Load())
    throughputWrite := float64(writes.Load()) / float64(elapsed.Seconds())
    log.Printf("%f writes/s\n", throughputWrite)
}

func cleanup() {
    os.RemoveAll("./test.db")
    os.RemoveAll("./test.db-shm")
    os.RemoveAll("./test.db-wal")
}

func setupDB(db *sql.DB) (err error) {
    tx, err := db.Begin()
    if err != nil {
        log.Fatal(err)
    }
    defer tx.Rollback()

    timestamp := time.Now().UTC().UnixMilli()
    for i := range 5_000_000 {
        recordID := uuid.Must(uuid.NewV7())
        _, err = tx.Exec(`INSERT INTO test
                 (id, timestamp, counter) VALUES (?, ?, ?)`, recordID[:], timestamp, i)
        if err != nil {
            return err
        }

        // insert by batches of 500,000 rows
      if i%500_000 == 0 {
            err = tx.Commit()
            if err != nil {
                tx.Rollback()
                return err
            }
            tx, err = db.Begin()
            if err != nil {
                return err
            }
        }
    }

    err = tx.Commit()
    if err != nil {
        return err
    }

    _, err = db.Exec("VACUUM")
    if err != nil {
        return err
    }

    _, err = db.Exec("ANALYZE")
    if err != nil {
        log.Fatal(err)
    }

    return nil
}
```
