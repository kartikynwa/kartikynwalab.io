+++
title = "Harry Potter and the Cursed Pasta"
date = 2021-11-17

[taxonomies]
Tags=["political"]
+++

“You’re a wizard, Harry,” Hagrid said. “And you’re coming to Hogwarts.”

“What’s Hogwarts?” Harry asked.

“It’s wizard school.”

“It’s not a public school, is it?”

“No, it’s privately run.”

“Good. Then I accept. Children are not the property of the state; everyone who
wishes to do so has the right to offer educational goods or services at a fair
market rate. Let us leave at once.”

---

“Malfoy bought the whole team brand-new Nimbus Cleansweeps!” Ron said, like a
poor person. “That’s not fair!”

“Everything that is possible is fair,” Harry reminded him gently. “If he is
able to purchase better equipment, that is his right as an individual. How is
Draco’s superior purchasing ability qualitatively different from my superior
Snitch-catching ability?”

“I guess it isn’t,” Ron said crossly.

Harry laughed, cool and remote, like if a mountain were to laugh. “Someday
you’ll understand, Ron.”

---

Professor Snape stood at the front of the room, his beak-like nose protruding
over the silent classroom. “There will be no foolish wand-waving or silly
incantations in this class. As such, I don’t expect many of you to appreciate
the subtle science and exact art that is potion-making. However, for those
select few who possess, the predisposition…I can teach you how to bewitch the
mind and ensnare the senses. I can tell you how to bottle fame, brew glory, and
even put a stopper in death.”

Harry’s hand shot up.

“What is it, Potter?” Snape asked, irritated.

“What’s the value of these potions on the open market?”

“What?”

“Why are you teaching children how to make these valuable products for
ourselves at a school teacher’s salary instead of creating products to meet
modern demand?”

“You impertinent boy–“

“Conversely, what’s to stop me from selling these potions myself after you
teach us how to master them?”

“I–“

“This is really more of a question for the Economics of Potion-Making, I guess.
What time are econ lessons here?”

“We have no economics lessons in this school, you ridiculous boy.”

Harry Potter stood up bravely. “We do now. Come with me if you want to learn
about market forces!”

The students poured into the hallway after him. They had a leader at last.
Dumbledore's army of economic analysts had been founded.

---

Harry and Ron stood before the Mirror of Erised. “My God,” Ron said. “Harry,
it’s your dead parents.”

Harry’s eyes flicked momentarily over to the mirror. “So it is. This
information is neither useful nor productive. Let us leave at once, to assist
Hagrid in his noble enterprise of raising as many dragon eggs as he sees fit,
in spite of our country’s unjust dragon-trading restrictions.”

“But it’s your parents, Harry,” Ron said. Ron never really got it.

Harry sighed. “The fundamental standard for all relationships is the trader
principle, Ron.”

“I don’t understand,” Ron said.

“Of course you don’t,” said Harry affectionately. “This principle holds that we
should interact with people on the basis of the values we can trade with them –
values of all sorts, including common interests in art, sports or music,
similar philosophical outlooks, political beliefs, sense of life, and more.
Dead people have no value according to the trader principle.”

“But they gave birth to y–“

“I made myself, Ron,” Harry said firmly.

---

“Give me your wand, boy,” Voldemort hissed.

“I cannot do that. This wand represents my wealth, which is itself a tangible
result of my achievements. Wealth is the product of man’s capacity to think,”
Harry said bravely.

Voldemort gasped.

“There is a level of cowardice lower than that of the conformist: the
fashionable non-conformist.”

Voldemort began to melt. Harry lit a cigarette, because he was the master of
fire.

“The smallest minority on earth is the individual. Those who deny individual
rights cannot claim to be defenders of minorities. The minimum wage is a tax on
the successful. The market will naturally dictate the minimum wage without the
government stepping in to determine arbitrary limits.”

Voldemort howled.

“I’m going to sell copies of my wand at an enormous markup,” Harry said, “and
you can buy one like everyone else.”

Voldemort had been defeated.

“He hated us for our freedom,” Ron said.

“No, Ron,” Harry said. “He hated us for our free markets.”

---

Hermione ached with desire for the both of them to master her, but nobody paid
her any attention.

"Girls are a waste of time Ron" explained Harry. "Young men spend time chasing
them that they could spend on self improvement instead. At least that's what
Jordan Peterson says. And Jordan knows everything. By the time I've read a self
improvement book and listened to the Joe Rogan podcast, brief manual
stimulation to a selection of pornographic videos is the only intimacy I
require."

"But Harry... I'm so lonely.. all of the time.. and I think it's driving me
mad.."

"Life is mad Ron. True sanity can only be found in the pursuit of nature. And
whilst primitively mankind's biological needs are to reproduce and survive,
mankind has evolved. Poverty is our only predator, wealth our one sanctity from
the forlorn rags of growing old."

"I dunno Harry.. I think I'd feel a lot better about life if I was shagging
Hermione"

"Trust me, Ron, when your expansive knowledge of financial markets lands you a
top internship at Gringotts the girls will be forming a queue. Forget Hermione,
forget all of the girls at this school. As apex predators we will be able to
mate with partners of our choosing."

They stood in silence now on the top of the astronomy tower. A soft breeze
rustled the autumn leaves on the ground below and scattered them silently over
the Great Lake. The giant squid swam near the shore, intermittently breaking
the surface of the water with a strong crash before returning to the depths
below. Ron looked out over the long, long skies of Hogwarts and dreamt of love
and romance and the soft touch of woman. He had learned that a man can only be
an economist for so long before he longs to be a man again and for music and
dancing and girls. Harry began to explain how using game theory he had decided
that the Patel twins would make the optimal dates to the Yule Ball and Ron
sighed silently. But Harry needed him. His parents were both dead and he'd be
all on his own otherwise. And so, in the Gryffindor boys' dormitory, when the
sun goes down at Hogwarts and the immense castle becomes black and silent
except for a flicker of a candle and a shadow on a wall, Ron Weasley dreams of
Hermione Granger.
