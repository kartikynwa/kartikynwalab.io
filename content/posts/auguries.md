+++
title = "The Auguries of Science"
date = "2021-11-06"

[taxonomies]
Tags=["non-political"]
+++

*The following is the last chapter of the book \'The Origin of
Consciousness in the Breakdown of the Bicameral Mind\' by Julian
Jaynes.*

I have tried in these few heterogeneous chapters of Book III to explain
as well as I could how certain features of our recent world, namely, the
social institutions of oracles and religions, and the psychological
phenomena of possession, hypnosis, and schizophrenia, as well as
artistic practices such as poetry and music, how all these can be
interpreted in part as vestiges of an earlier organization of human
nature. These are not in any sense a complete catalogue of the present
possible projections from our earlier mentality. They are simply some of
the most obvious. And the study of their interaction with the developing
consciousness continually laying siege to them allows us an
understanding that we would not otherwise have.

In this final chapter, I wish to turn to science itself and point out
that it too, and even my entire essay, can be read as a response to the
breakdown of the bicameral mind. For what is the nature of this blessing
of certainty that science so devoutly demands in its very Jacob-like
wrestling with nature? Why should we demand that the universe make
itself clear to us? Why do we care?

To be sure, a part of the impulse to science is simple curiosity, to
hold the unheld and watch the unwatched. We are all children in the
unknown. It is no reaction to the loss of an earlier mentality to
delight in the revelations of the electron miscroscope or in quarks or
in negative gravity in black holes among the stars. Technology is a
second and even more sustaining source of the scientific ritual,
carrying its scientific basis forward on its own increasing and
uncontrollable momentum through history. And perhaps a deep aptic
structure for hunting, for bringing a problem to bay, adds its
motivational effluence to the pursuit of truth.

But over and behind these and other causes of science has been something
more universal, something in this age of specialization often unspoken.
It is something about understanding the totality of existence, the
essential defining reality of things, the entire universe and man\'s
place in it. It is a groping among stars for final answers, a wandering
the infinitesimal for the infinitely general, a deeper and deeper
pilgrimage into the unknown. It is a direction whose far beginning in
the mists of history can be distantly seen in the search for lost
directives in the breakdown of the bicameral mind.

It is a search that is obvious in the omen literature of Assyria where,
as we saw in II.4, science begins. It is also obvious a mere half
millennium later when Pythagoras in Greece is seeking the lost
invariants of life in a theology of divine numbers and their
relationships, thus beginning the science of mathematics. And so through
two millennia, until, with a motivation not different, Galileo calls
mathematics the speech of God, or Pascal and Leibnitz echo him, saying
they hear God in the awesome rectitudes of mathematics.

We sometimes think, and even like to think, that the two greatest
exertions that have influenced mankind, religion and science, have
always been historical enemies, intriguing us in opposite directions.
But this effort at special identity is loudly false. It is not religion
but the church and science that were hostile to each other. And it was
rivalry, not contravention. Both were religious. They were two giants
fuming at each other over the same ground. Both proclaimed to be the
only way to divine revelation.

It was a competition that first came into absolute focus with the late
Renaissance, particularly in the imprisonment of Galileo in 1633. The
stated and superficial reason was that his publications had not been
first stamped with papal approval. But the true argument, I am sure, was
no such trivial surface event. For the writings in question were simply
the Copernican heliocentric theory of the solar system which had been
published a century earlier by a churchman without any fuss whatever.
The real division was more profound and can, I think, only be understood
as a part of the urgency behind mankind\'s yearning for divine
certainties. The real chasm was between the political authority of the
church and the individual authority of experience. And the real question
was whether we are to find our lost authorization through an apostolic
succession from ancient prophets who heard divine voices, or through
searching the heavens of our own experience right now in the objective
world without any priestly intercession. As we all know, the latter
became Protestantism and, in its rationalist aspect, what we have come
to call the Scientific Revolution.

If we would understand the Scientific Revolution correctly, we should
always remember that its most powerful impetus was the unremitting
search for hidden divinity. As such, it is a direct descendant of the
breakdown of the bicameral mind. In the late seventeenth century, to
choose an obvious example, it is three English Protestants, all amateur
theologians and fervently devout, who build the foundations for physics,
psychology, and biology: the paranoiac Isaac Newton writing down God\'s
speech in the great universal laws of celestial gravitation; the gaunt
and literal John Locke knowing his Most Knowing Being in the riches of
knowing experience; and the peripatetic John Ray, an unkempt
ecclesiastic out of a pulpit, joyfully limning the Word of his Creator
in the perfection of the design of animal and plant life. Without this
religious motivation, science would have been mere technology, limping
along on economic necessity.

The next century is complicated by the rationalism of the Enlightenment,
whose main force I shall come to in a moment. But in the great shadow of
the Enlightenment, science continued to be bound up in this spell of the
search for divine authorship. Its most explicit statement came in what
was called Deism, or in Germany, Vernunftreligion. It threw away the
church\'s \"Word,\" despised its priests, mocked altar and sacrament,
and earnestly preached the reaching of God through reason and science.
The whole universe is an epiphany! God is right out here in Nature under
the stars to be talked with and heard brilliantly in all the grandeur of
reason, rather than behind the rood screens of ignorance in the murky
mutterings of costumed priests.

Not that such scientific deists were in universal agreement. For some,
like the apostle-hating Reimarus, the modern founder of the science of
animal behavior, animal triebe or drives were actually the thoughts of
God and their perfect variety his very mind. Whereas for others, like
the physicist. Maupertuis, God cared little about any such meaningless
variety of phenomena; he lived only in pure abstractions, in the great
general laws of Nature which human reason, with the fine devotions of
mathematics, could discern behind such variety. Indeed, the tough-minded
materialist scientist today will feel uncomfortable with the fact that
science in such divergent and various directions only two centuries ago
was a religious endeavor, sharing the same striving as the ancient
psalms, the effort to once again see the elohim \"face to face.\"   This
drama, this immense scenario in which humanity has been performing on
this planet over the last 4000 years, is clear when we take the large
view of the central intellectual tendency of world history. In the
second millennium B.C., those of us who still heard the voices, our
oracles and prophets, they too died away. In the first millennium a.d.,
it is their sayings and hearings preserved in sacred texts through which
we obeyed our lost divinities. And in the second millennium a.d., these
writings lose their authority. The Scientific Revolution turns us away
from the older sayings to discover the lost authorization in Nature.
What we have been through in these last four millennia is the slow
inexorable profaning of our species. And in the last part of the second
millennium a.d., that process is apparently becoming complete. It is the
Great Human Irony of our noblest and greatest endeavor on this planet
that in the quest for authorization, in our reading of the language of
God in Nature, we should read there so clearly that we have been so
mistaken.

This secularization of science, which is now a plain fact, is certainly
rooted in the French Enlightenment which I have just alluded to. But it
became rough and earnest in 1842 in Germany in a famous manifesto by
four brilliant young physiologists. They signed it like pirates,
actually in their own blood. Fed up with Hegelian idealism and its
pseudoreligious interpretations of material matters, they angrily
resolved that no forces other than common physicochemical ones would be
considered in their scientific activity. No spiritual entities. No
divine substances. No vital forces. This was the most coherent and
shrill statement of scientific materialism up to that time. And
enormously influential.

Five years later, one of their group, the famous physicist and
psychologist Hermann von Helmholtz, proclaimed his Principle of the
Conservation of Energy. Joule had said it more kindly, that \"the Great
Agents of Nature are indestructible,\" that sea and sun and coal and
thunder and heat and wind are one energy and eternal. But Helmholtz
abhorred the mush of the Romantic. His mathematical treatment of the
principle coldly placed the emphasis where it has been ever since: there
are no outside forces in our closed world of energy transformations.
There is no comer in the stars for any god, no crack in this closed
universe of matter for any divine influence to seep through, none
whatever.

All this might have respectfully stayed back simply as a mere working
tenet for Science, had it not been for an even more stunning profaning
of the idea of the holy in human affairs that followed immediately. It
was particularly stunning because it came from within the very ranks of
religiously motivated science. In Britain since the seventeenth century,
the study of what was called \"natural history\" was commonly the
consoling joy of finding the perfections of a benevolent Creator in
nature. What more devastation could be heaped upon these tender
motivations and consolations than the twin announcement by two of their
own midst, Darwin and Wallace, both amateur naturalists in the grand
manner, that it was evolution, not a divine intelligence, that has
created all nature. This too had been put earlier in a kindlier way by
others, such as Darwin\'s grandfather, Erasmus Darwin, or Lamarck, or
Robert Chambers, or even in the exaltations of an Emerson or a Goethe.
But the new emphasis was dazzling strong and unrelieving. Cold
calculating chance, by making some able to survive better in this
wrestle for life, and so to reproduce more, generation after generation,
has blindly, even cruelly, carved this human species out of matter, mere
matter. When combined with German materialism, as it was in the wantonly
abrasive Huxley, as we saw in the Introduction to this essay, the theory
of evolution by natural selection was the hollowing knell of all that
ennobling tradition of man as the purposed creation of Majestic
Greatnesses, the elohim, that goes straight back into the unconscious
depths of the Bicameral Age. It said in a word that there is no
authorization from outside. Behold! there is nothing there. What we must
do must come from ourselves. The king at Eynan can stop staring at Mount
Hermon; the dead king can die at last. We, we fragile human species at
the end of the second millennium A.D., we must become our own
authorization. And here at the end of the second millennium and about to
enter the third, we are surrounded with this problem. It is one that the
new millennium will be working out, perhaps slowly, perhaps swiftly,
perhaps even with some further changes in our mentality.

The erosion of the religious view of man in these last years of the
second millennium is still a part of the breakdown of the bicameral
mind. It is slowly working serious changes in every fold and field of
life. In the competition for membership among religious bodies today, it
is the older orthodox positions, ritually closer to the long apostolic
succession into the bicameral past, that are most diminished by
conscious logic. The changes in the Catholic Church since Vatican II can
certainly be scanned in terms of this long retreat from the sacred which
has followed the inception of consciousness into the human species. The
decay of religious collective cognitive imperatives under the pressures
of rationalist science, provoking, as it does, revision after revision
of traditional theological concepts, cannot sustain the metaphoric
meaning behind ritual. Rituals are behavioral metaphors, belief acted,
divination foretold, exopsychic thinking. Rituals are mnemonic devices
for the great narratizations at the heart of church life. And when they
are emptied out into cults of spontaneity and drained of their high
seriousness, when they are acted unfelt and reasoned at with
irresponsible objectivity, the center is gone and the widening gyres
begin. The result in this age of communications has been worldwide:
liturgy loosened into the casual, awe softening in relevance, and the
washing out of that identity-giving historical definition that told man
what he was and what he should be. These sad temporizings, often begun
by a bewildered clergy, do but encourage the great historical tide they
are designed to deflect. Our paralogical compliance to verbally mediated
reality is diminished: we crash into chairs in our way, not go around
them; we will be mute rather than say we do not understand our speech;
we will insist on simple location. It is the divine tragedy or the
profane comedy depending on whether we would be purged of the past or
quickened into the future.

What happens in this modern dissolution of ecclesiastical authorization
reminds us a little of what happened long ago after the breakdown of the
bicameral mind itself. Everywhere in the contemporary world there are
substitutes, other methods of authorization. Some are revivals of
ancient ones: the popularity of possession religions in South America,
where the church had once been so strong; extreme religious absolutism
ego-based on \"the Spirit,\" which is really the ascension of Paul over
Jesus; an alarming rise in the serious acceptance of astrology, that
direct heritage from the period of the breakdown of the bicameral mind
in the Near East; or the more minor divination of the I Ching also a
direct heritage from the period just after the breakdown in China. There
are also the huge commercial and sometimes psychological successes of
various meditation procedures, sensitivity training groups, mind
control, and group encounter practices. Other persuasions often seem
like escapes from a new boredom of unbelief, but are also characterized
by this search for authorization: faiths in various pseudosciences, as
in Scientology, or in unidentified flying objects bringing authority
from other parts of our universe, or that gods were at one time actually
such visitors; or the stubborn muddled fascination with extrasensory
perception as a supposed demonstration of a spiritual surround of our
lives whence some authorization might come; or the use of psychotropic
drugs as ways of contacting profounder realities, as they were for most
of the American native Indian civilizations in the breakdown of their
bicameral mind. Just as we saw in III.2 that the collapse of the
institutionalized oracles resulted in smaller cults of induced
possession, so the waning of institutional religions is resulting in
these smaller, more private religions of every description. And this
historical process can be expected to increase the rest of this century.

Nor can we say that modern science itself is exempt from a similar
patterning. For the modern intellectual landscape is informed with the
same needs, and often in its larger contours goes through the same
quasi-religious gestures, though in a slightly disguised form. These
scientisms, as I shall call them, are clusters of scientific ideas which
come together and almost surprise themselves into creeds of belief,
scientific mythologies which fill the very felt void left by the divorce
of science and religion in our time. They differ from classical science
and its common debates in the way they evoke the same response as did
the religions which they seek to supplant. And they share with religions
many of their most obvious characteristics: a rational splendor that
explains everything, a charismatic leader or succession of leaders who
are highly visible and beyond criticism, a series of canonical texts
which are somehow outside the usual arena of scientific criticism,
certain gestures of idea and rituals of interpretation, and a
requirement of total commitment. In return the adherent receives what
the religions had once given him more universally: a world view, a
hierarchy of importances, and an auguring place where he may find out
what to do and think, in short, a total explanation of man. And this
totality is obtained not by actually explaining everything, but by an
encasement of its activity, a severe and absolute restriction of
attention, such that everything that is not explained is not in view.

The materialism I have just mentioned was one of the first such
scientisms. Scientists in the middle of the nineteenth century were
almost numbed with excitement by dramatic discoveries of how nutrition
could change the bodies and minds of men. And so it became a movement
called Medical Materialism, identified with relieving poverty and pain,
taking to itself some of the forms and all of the fervor of the
religions eroding around it. It captured the most exciting minds of its
generation, and its program sounds distantly familiar: education, not
prayers; nutrition, not communion; medicine, not love; and politics, not
preaching.

Distantly familiar because Medical Materialism, still haunted with
Hegel, matured in Marx and Engels into dialectical materialism,
gathering to itself even more of the ecclesiastical forms of the outworn
faiths around it. Its central superstition then, as now, is that of the
class struggle, a kind of divination which gives a total explanation of
the past and predecides what to do in every office and alarm of life.
And even though ethnicism, nationalism, and unionism, those collective
identity markers of modern man, have long ago showed the mythical
character of the class struggle, still Marxism today is joining armies
of millions into battle to erect the most authoritarian states the world
has ever seen.

In the medical sciences, the most prominent scientism, I think, has been
psychoanalysis. Its central superstition is repressed childhood
sexuality. The handful of early cases of hysteria which could be so
interpreted become the metaphiers by which to understand all personality
and art, all civilization and its discontents. And it too, like Marxism,
demands total commitment, initiation procedures, a worshipful relation
to its canonical texts, and gives in return that same assistance in
decision and direction in life which a few centuries ago was the
province of religion.

And, to take an example closer to my own tradition, I will add
behaviorism. For it too has its central auguring place in a handful of
rat and pigeon experiments, making them the metaphiers of all behavior
and history. It too gives to the individual adherent the talisman of
control by reinforcement contingencies by which he is to meet his world
and understand its vagaries. And even though the radical
environmentalism behind it, of belief in a tabula rasa organism that can
be built up into anything by reinforcement has long been known to be
questionable, given the biologically evolved aptic structuring of each
organism, these principles still draw adherents into the hope of a new
society based upon such control.

Of course these scientisms about man begin with something that is true.
That nutrition can improve health both of mind and body is true. The
class struggle as Marx studied it in the France of Louis Napoleon was a
fact. The relief of hysterical symptoms in a few patients by analysis of
sexual memories probably happened. And hungry animals or anxious men
certainly will learn instrumental responses for food or approbation.
These are true facts. But so is the shape of a liver of a sacrificed
animal a true fact. And so the Ascendants and Midheavens of astrologers,
or the shape of oil on water. Applied to the world as representative of
all the world, facts become superstitions. A superstition is after all
only a metaphier grown wild to serve a need to know. Like the entrails
of animals or the flights of birds, such scientistic superstitions
become the preserved ritualized places where we may read out the past
and future of man, and hear the answers that can authorize our actions.

Science then, for all its pomp of factness, is not unlike some of the
more easily disparaged outbreaks of pseudoreligions. In this period of
transition from its religious basis, science often shares with the
celestial maps of astrology, or a hundred other irrationalisms, the same
nostalgia for the Final Answer, the One Truth, the Single Cause. In the
frustrations and sweat of laboratories, it feels the same temptations to
swarm into sects, even as did the Khabiru refugees, and set out here and
there through the dry Sinais of parched fact for some rich and brave
significance flowing with truth and exaltation. And all of this, my
metaphor and all, is a part of this transitional period after the
breakdown of the bicameral mind.

And this essay is no exception.

Curiously, none of these contemporary movements tells us anything about
what we are supposed to be like after the wrinkles in our nutrition have
been ironed smooth, or \"the withering away of the state\" has occurred,
or our libidos have been properly cathected, or the chaos of
reinforcements has been made straight. Instead their allusion is mostly
backward, telling us what has gone wrong, hinting of some cosmic
disgrace, some earlier stunting of our potential. It is, I think, yet
another characteristic of the religious form which such movements have
taken over in the emptiness caused by the retreat of ecclesiastical
certainty---that of a supposed fall of man.

This strange and, I think, spurious idea of a lost innocence takes its
mark precisely in the breakdown of the bicameral mind as the first great
conscious narratization of mankind. It is the song of the Assyrian
psalms, the wail of the Hebrew hymns, the myth of Eden, the fundamental
fall from divine favor that is the source and first premise of the
world\'s great religions. I interpret this hypothetical fall of man to
be the groping of newly conscious men to narratize what has happened to
them, the loss of divine voices and assurances in a chaos of human
directive and selfish privacies.

We see this theme of lost certainty and splendor not only stated by all
the religions of man throughout history, but also again and again even
in nonreligious intellectual history. It is there from the reminiscence
theory of the Platonic Dialogues, that everything new is really a
recalling of a lost better world, all the way to Rousseau\'s complaint
of the corruption of natural man by the artificialities of civilization.
And we see it also in the modern scientisms I have mentioned: in Marx\'s
assumption of a lost \"social childhood of mankind where mankind unfolds
in complete beauty,\" so clearly stated in his earlier writings, an
innocence corrupted by money, a paradise to be regained. Or in the
Freudian emphasis on the deep-seatedness of neurosis in civilization and
of dreadful primordial acts and wishes in both our racial and individual
pasts; and by inference a previous innocence, quite unspecified, to
which we return through psychoanalysis. Or in behaviorism, if less
distinctly, in the undocumented faith that it is the chaotic
reinforcements of development and the social process that must be
controlled and ordered to return man to a quite unspecified ideal before
these reinforcements had twisted his true nature awry.

I therefore believe that these and many other movements of our time are
in the great long picture of our civilizations related to the loss of an
earlier organization of human natures. They are attempts to return to
what is no longer there, like poets to their inexistent Muses, and as
such they are characteristic of these transitional millennia in which we
are imbedded.

I do not mean that the individual thinker, the reader of this page or
its writer, or Galileo or Marx, is so abject a creature as to have any
conscious articulate willing to reach either the absolutes of gods or to
return to a preconscious innocence. Such terms are meaningless applied
to individual lives and removed from the larger context of history. It
is only if we make generations our persons and centuries hours that the
pattern is clear.

As individuals we are at the mercies of our own collective imperatives.
We see over our everyday attentions, our gardens and politics, and
children, into the forms of our culture darkly. And our culture is our
history. In our attempts to communicate or to persuade or simply
interest others, we are using and moving about through cultural models
among whose differences we may select, but from whose totality we cannot
escape. And it is in this sense of the forms of appeal, of begetting
hope or interest or appreciation or praise for ourselves or for our
ideas, that our communications are shaped into these historical
patterns, these grooves of persuasion which are even in the act of
communication an inherent part of what is communicated. And this essay
is no exception.

No exception at all. It began in what seemed in my personal
narratizations as an individual choice of a problem with which I have
had an intense involvement for most of my life: the problem of the
nature and origin of all this invisible country of touchless
rememberings and unshowable reveries, this introcosm that is more myself
than anything I can find in any mirror. But was this impulse to discover
the source of consciousness what it appeared to me? The very notion of
truth is a culturally given direction, a part of the pervasive nostalgia
for an earlier certainty. The very idea of a universal stability, an
eternal firmness of principle out there that can be sought for through
the world as might an Arthurian knight for the Grail, is, in the
morphology of history, a direct outgrowth of the search for lost gods in
the first two millennia after the decline of the bicameral mind. What
was then an augury for direction of action among the ruins of an archaic
mentality is now the search for an innocence of certainty among the
mythologies of facts.
