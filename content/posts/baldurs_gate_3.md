+++
title = "Baldur's Gate 3"
date = "2023-11-22"

[taxonomies]
Tags=["non-political"]
+++

# Baldur's Gate 3

I hadn't played the previous two installments and neither do I plan to because of how old they are.
I also don't know what happened in these games, so I don't know how the events of BG3 relate to its
predecessors'. So, for me at least, it is not different from a standalone game.

BG3 is developed by Larian Studios. They are well-known for their RPG games, among which the only
one I have played is /Divinity II: Original Sin/ and that too only briefly. BG3 is built upon the
foundations laid by their previous RPG games so I kind of knew what to expect. I really was not
looking forward to the combat. Those who like turn-based combat seem to like it but it's just not
for me. Regardless, I was a bit excited for the game because it was hyped and was a massive success
upon release.

I ended up pirating the GOG copy of the game. To make combat less of an issue, I played the game on
the Explorer difficulty. This allowed me to focus more on the story and the cinematic aspects of the
game.

Overall, the game is excellent. The RPG system being used is Dungeons and Dragons' 5th Edition. The
game does not do any hand-holding which is a bit sad. I still only loosely understand what an action
and a bonus action are. Leveling up the characters is a bit of a nightmare because of choice
paralysis. They really should have some auto-leveling and cookie-cutter builds especially for the
companion characters. I ended up copying the builds from a YouTube video which was a little painful
because YouTubers are annoying.

The characters in the game are great. The writing is great. The voice acting is great. The plot is
alright too. I don't like Ilithid/Mind Flayers because they don't feel like they belong in the
setting. They feel more sci-fi than fantasy. And they feature heavily. But it's a minor gripe.

The combat is great too. Even though I am not in a position to appreciate it in its full depths, I
liked what I saw. On the Explorer difficulty, major battles were sometimes challenging. Two that I
remember struggling with were:
- Yurgir
- That Bhaal groupie trying to kill the Facemaker. I took a hiatus from the game because this fight
  had me little stumped. Also I could not figure out how to save the Facemaker. Taking hints from a
  YouTube video, once I restarted the game, the fight was pretty manageable so I am not sure.

In hindsight, it is the invisibility that is very annoying to deal with. My avatar character even
had the false eye that grants invisibility yet it rarely works when I really need it to. I guess it
benefits from a certain stat that was low for the wearer.

What also makes the game truly excellent is how effectual the choices are. It is hard to describe
the breadth of the variety but it is truly staggering. While the major plot remains pretty much the
same (to the extent of my knowledge) where you have to fight the Elder Brain (or the Netherbrain)
towards the end, the journey to this end along with the ending itself can vary widely made on the
choices made.

It is evident why the game is a huge hit.

Unfortunately, I am one-hundred-percent sure that the video gaming "industry" as a whole learn zero
lessons from this game. Maybe Larian will make more games like this. But the blood-sucking parasites
which comprise the "industry" will continue to suck blood and pathologically churn out garbage. The
development cycle of Baldur's Gate 3 was a bit different from a typical "AAA" game because the game
was out in early access for a long time and constantly iterated on player feedback. I don't know if
the whole was accessible during early access or not. Either way, while the game was great and is
widely acknowledged as so, it is and is going to remain an anomaly.
